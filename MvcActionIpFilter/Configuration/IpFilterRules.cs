﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MvcActionIpFilter.Configuration {
    public class IpFilterRules : ConfigurationElementCollection {
        public IpFilterRules() {
            AddElementName = "rule";
        }

        protected override ConfigurationElement CreateNewElement() {
            return new IpFilterRule();
        }

        protected override object GetElementKey(ConfigurationElement element) {
            return ((IpFilterRule)element).Name;
        }
    }
}
