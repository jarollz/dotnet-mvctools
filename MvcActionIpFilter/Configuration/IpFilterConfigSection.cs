﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MvcActionIpFilter.Configuration {
    public class IpFilterConfigSection : ConfigurationSection {
        [ConfigurationProperty("rules", IsDefaultCollection = false)]
        public IpFilterRules Rules {
            get { return (IpFilterRules)base["rules"]; }
        }
    }
}
