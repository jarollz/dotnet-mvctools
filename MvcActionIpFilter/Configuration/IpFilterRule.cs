﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MvcActionIpFilter.Configuration {
    public class IpFilterRule : ConfigurationElementCollection {
        public IpFilterRule() {
            AddElementName = "pattern";
        }

        [ConfigurationProperty("name", IsRequired = true, IsKey = true)]
        public string Name {
            get { return (string)this["name"]; }
            set { this["name"] = value; }
        }

        [ConfigurationProperty("isEnabled", IsRequired = true)]
        public bool IsEnabled {
            get { return (bool)this["isEnabled"]; }
            set { this["isEnabled"] = value; }
        }

        [ConfigurationProperty("mode", IsRequired = true)]
        public IpFilterRuleMode Mode {
            get { return (IpFilterRuleMode)this["mode"]; }
            set { this["mode"] = value; }
        }

        protected override ConfigurationElement CreateNewElement() {
            return new IpPattern();
        }

        protected override object GetElementKey(ConfigurationElement element) {
            return ((IpPattern)element).Regex;
        }
    }

    public enum IpFilterRuleMode {
        Whitelist, Blacklist
    }
}
