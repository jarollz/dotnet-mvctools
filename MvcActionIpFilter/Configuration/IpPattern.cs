﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MvcActionIpFilter.Configuration {
    public class IpPattern : ConfigurationElement {
        [ConfigurationProperty("regex", IsRequired = true, IsKey = true)]
        public string Regex {
            get { return (string)this["regex"]; }
            set { this["regex"] = value; }
        }

        [ConfigurationProperty("isEnabled", IsRequired = true)]
        public bool IsEnabled {
            get { return (bool)this["isEnabled"]; }
            set { this["isEnabled"] = value; }
        }
    }
}
