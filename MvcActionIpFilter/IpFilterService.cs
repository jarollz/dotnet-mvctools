﻿using MvcActionIpFilter.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Web;

namespace MvcActionIpFilter {
    /// <summary>
    /// Provide API for others to check IP filter allowed status
    /// </summary>
    public static class IpFilterService {

        internal const string ERROR_MESSAGE_FORMAT_ON_RULE_NAME_NOT_FOUND = "Can't find ip filter rule '{0}' from the web app config.";

        /// <summary>
        /// Check whether an IP is allowed or not
        /// </summary>
        /// <param name="ruleName">the rule name to consider for ip patterns</param>
        /// <param name="ipAddress">the ip address to check, if null, use current connected client ip address</param>
        /// <returns>true if allowed</returns>
        public static bool IsAllowed(string ruleName, string ipAddress = null) {
            var isAllowed = true;
            var hasRuleMatch = false;

            var httpContext = new HttpContextWrapper(HttpContext.Current);
            var visitorIp = ipAddress == null ? GetVisitorIp(httpContext) : ipAddress.Trim();

            foreach (IpFilterRule rule in Settings.ConfigSection.Rules) {
                if (!rule.IsEnabled)
                    continue;

                if (rule.Name == ruleName) {
                    hasRuleMatch = true;
                    var hasPatternMatch = false;
                    foreach (IpPattern pattern in rule) {
                        if (!pattern.IsEnabled)
                            continue;

                        var regx = new Regex(pattern.Regex);
                        if (regx.IsMatch(visitorIp)) {
                            hasPatternMatch = true;
                            break;
                        }
                    }

                    if ((!hasPatternMatch && rule.Mode == IpFilterRuleMode.Whitelist) ||
                        (hasPatternMatch && rule.Mode == IpFilterRuleMode.Blacklist)) {
                        isAllowed = false;
                    }

                    break;
                }
            }

            if (!hasRuleMatch) {
                throw new InvalidOperationException(string.Format(ERROR_MESSAGE_FORMAT_ON_RULE_NAME_NOT_FOUND, ruleName));
            }

            return isAllowed;
        }

        /// <summary>
        /// Get visitor ip address
        /// </summary>
        /// <param name="httpContext">HttpContextBase object</param>
        /// <returns>ip address</returns>
        public static string GetVisitorIp(HttpContextBase httpContext) {
            string ip = null;
            var requestIsLocal = false;
            if (httpContext != null) { // ASP.NET
                requestIsLocal = httpContext.Request.IsLocal;
                ip = string.IsNullOrEmpty(httpContext.Request.ServerVariables["HTTP_X_FORWARDED_FOR"])
                    ? httpContext.Request.UserHostAddress
                    : httpContext.Request.ServerVariables["HTTP_X_FORWARDED_FOR"];
            }
            if (string.IsNullOrEmpty(ip) || ((ip.Trim() == "::1" || ip.Trim() == "127.0.0.1") && !requestIsLocal)) { // still can't decide or is LAN
                var lan = Dns.GetHostEntry(Dns.GetHostName()).AddressList.FirstOrDefault(r => r.AddressFamily == AddressFamily.InterNetwork);
                ip = lan == null ? string.Empty : lan.ToString();
            }
            return ip;
        }
    }
}
