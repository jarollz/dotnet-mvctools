﻿using MvcActionIpFilter.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Configuration;

namespace MvcActionIpFilter {
    public static class Settings {
        public const string DEFAULT_IPFILTER_SECTION_NAME = "mvcActionIpFilter";
        public const string SECTION_NAME_SETTING = "MvcActionIpFilter:SectionName";

        public static string IpFilterSectionName {
            get {
                var appStg = WebConfigurationManager.AppSettings[SECTION_NAME_SETTING];
                if (appStg != null && appStg.Trim().Length > 0) {
                    return appStg;
                }
                return DEFAULT_IPFILTER_SECTION_NAME;
            }
        }
        
        public static IpFilterConfigSection ConfigSection {
            get {
                if (_ConfigSection == null) {
                    _ConfigSection = WebConfigurationManager.GetSection(IpFilterSectionName) as IpFilterConfigSection;
                }
                return _ConfigSection;
            }
        }
        private static IpFilterConfigSection _ConfigSection;
    }
}
