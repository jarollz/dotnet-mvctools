﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Configuration;
using System.Web.Mvc;

namespace MvcActionIpFilter {
    public class ActionIpFilter : ActionFilterAttribute {

        public string RuleName { get; private set; }

        public ActionIpFilter(string ruleName) {
            RuleName = ruleName;
        }

        public override void OnActionExecuting(ActionExecutingContext filterContext) {
            var isAllowed = true;

            try {
                isAllowed = IpFilterService.IsAllowed(RuleName);
            } catch (InvalidOperationException iox) {
                if (iox.Message == string.Format(IpFilterService.ERROR_MESSAGE_FORMAT_ON_RULE_NAME_NOT_FOUND, RuleName)) {
                    string actionName = filterContext.ActionDescriptor.ActionName;
                    string controllerName = filterContext.ActionDescriptor.ControllerDescriptor.ControllerName;
                    throw new InvalidOperationException(string.Format("ActionIpFilter is used but the given rule name isn't found in config (controller={0}, action={1}).", controllerName, actionName));
                } else {
                    throw iox;
                }
            }

            if (isAllowed) {
                base.OnActionExecuting(filterContext);
            } else {
                throw new HttpException(403, "Access is forbidden.");
            }
        }
    }
}
