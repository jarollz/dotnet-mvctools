﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ObjectTemplater.Infrastructure.Exts {
    internal static class StringExt {
        public static string EnsureStringIsTrimmedAndLowerCase(this string theString) {
            string rm = theString;
            if (rm != null) {
                rm = rm.Trim().ToLower();
                if (rm.Length > 0) {
                    return rm;
                } else {
                    throw new ArgumentException("The string after being trimmed must be non-empty.", "theString");
                }
            } else {
                throw new ArgumentException("The string can't be null.", "theString");
            }
        }
    }
}
