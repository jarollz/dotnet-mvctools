﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ObjectTemplater.Infrastructure.Exts {
    internal static class TypeExt {

        public static string ToTemplateName(this Type objType) {
            var nmsp = objType.Namespace;
            var nm = objType.Name;
            var genericTypeNameMarkerIndexPos = nm.IndexOf('`');
            var fullName = nmsp + ".";
            if (genericTypeNameMarkerIndexPos >= 0) {
                fullName = fullName + nm.Substring(0, nm.IndexOf('`'));
            } else {
                fullName = fullName + nm;
            }

            return fullName;
        }
    }
}
