﻿using ObjectTemplater.Infrastructure.Exts;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ObjectTemplater.Models {
    /// <summary>
    /// This is the default template file paths generator that can find suitable template for an object
    /// while considering its inheritance hierarchy and interfaces
    /// </summary>
    public class DefaultTemplateFilePathsGenerator : ITemplateFilePathsGenerator {
        
        public IEnumerable<string> GenerateTemplateFilePaths(
            object obj,
            string renderMode,
            IEnumerable<string> themeNames,
            string rootTemplateDirectory,
            string templateFileExtension) {

            return GenerateTemplateFilePaths(
                obj.GetType(),
                renderMode,
                themeNames,
                rootTemplateDirectory,
                templateFileExtension);
        }

        public IEnumerable<string> GenerateTemplateFilePaths(
            Type type,
            string renderMode,
            IEnumerable<string> themeNames,
            string rootTemplateDirectory,
            string templateFileExtension) {
            
            var nodesMap = new TypeTemplatePathsBuilder(type);
            var typeNamePaths = nodesMap.GenerateTemplatePaths();
            var possibleRenderMode = new string[] { renderMode, "" };

            foreach (var possibleTheme in themeNames) {
                foreach (var rm in possibleRenderMode) {
                    foreach (var typeNamePath in typeNamePaths) {
                        var templateFilePath = Path.Combine(
                                rootTemplateDirectory,
                                possibleTheme,
                                typeNamePath,
                                "_" + rm + "." + templateFileExtension
                            );
                        yield return templateFilePath;
                    }
                }
            }
        }
    }
}
