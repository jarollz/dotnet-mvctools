﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ObjectTemplater.Models {
    /// <summary>
    /// This class represents the theme configuration.
    /// It's mainly used to help the system determines alternative theme to consider
    /// when specific object template isn't found in the theme.
    /// </summary>
    public class ThemeConfiguration {
        /// <summary>
        /// Theme name
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// Theme pretty name.
        /// </summary>
        public string DisplayName { get; set; }
        /// <summary>
        /// Theme description.
        /// </summary>
        public string Description { get; set; }
        /// <summary>
        /// Theme author name
        /// </summary>
        public string Author { get; set; }
        /// <summary>
        /// Theme version
        /// </summary>
        public string Version { get; set; }
        /// <summary>
        /// An array of theme names that represents the alternatives to look at when a template
        /// for an object type isn't found in this theme.
        /// This array is sorted from highest to lowest priority and will always return a copy of
        /// it when accessed.
        /// Also, regardless of the content of this array, the default theme will always be
        /// considered last.
        /// </summary>
        public IEnumerable<string> Alternatives {
            get {
                var ret = _Alternatives.ToList();
                ret.Add(TemplaterConfiguration.DEFAULT_THEME_NAME);
                ret.Remove(Name);
                return ret;
            }
            set {
                if (value == null) {
                    _Alternatives = new LinkedList<string>();
                } else {
                    _Alternatives = new LinkedList<string>(value);
                    _Alternatives.Remove(TemplaterConfiguration.DEFAULT_THEME_NAME);
                }
            }
        }
        private LinkedList<string> _Alternatives;

        /// <summary>
        /// Create an instance of ThemeConfiguration
        /// </summary>
        internal ThemeConfiguration() {
            _Alternatives = new LinkedList<string>();
        }

        /// <summary>
        /// Add alternative theme to this configuration.
        /// </summary>
        /// <param name="alternativeThemeName">The alternative theme name</param>
        /// <param name="priority">The priority, lower number means higher priority</param>
        public void AddAlternativeTheme(string alternativeThemeName, int priority) {
            if (alternativeThemeName == TemplaterConfiguration.DEFAULT_THEME_NAME) return;

            var existing = _Alternatives.Where(alt => alt == alternativeThemeName).FirstOrDefault();
            if (existing != null) {
                _Alternatives.Remove(existing);
            }

            if (priority < 0) {
                _Alternatives.AddFirst(alternativeThemeName);
            } else if (priority >= _Alternatives.Count) {
                _Alternatives.AddLast(alternativeThemeName);
            } else {
                _Alternatives.AddBefore(
                    _Alternatives.Find(_Alternatives.ElementAt(priority)),
                    alternativeThemeName
                );
            }
        }

        /// <summary>
        /// Remove alternative theme from this configuration
        /// </summary>
        /// <param name="alternativeThemeName">The alternative theme name</param>
        public void RemoveAlternativeTheme(string alternativeThemeName) {
            _Alternatives.Remove(alternativeThemeName);
        }
    }
}
