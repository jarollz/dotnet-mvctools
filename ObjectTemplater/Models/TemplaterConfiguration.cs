﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using ObjectTemplater.Infrastructure.Exts;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ObjectTemplater.Models {
    /// <summary>
    /// This class is used to define ObjectTemplater configuration.
    /// </summary>
    public class TemplaterConfiguration {
        /// <summary>
        /// The fallback theme name.
        /// </summary>
        public const string DEFAULT_THEME_NAME = "default";

        /// <summary>
        /// The default render mode to use if not specified.
        /// </summary>
        public const string DEFAULT_RENDER_MODE = "view";

        /// <summary>
        /// The configuration file name for any theme.
        /// </summary>
        public const string THEME_CONFIGURATION_FILE_NAME = "theme.jsonconfig";

        /// <summary>
        /// The default template file extension. It defaults to cshtml.
        /// </summary>
        public const string DEFAULT_TEMPLATE_FILE_EXTENSION = "cshtml";

        /// <summary>
        /// The real full path of the template directory. All searching for correct template will use this directory as base.
        /// When setting this property, the string will be automatically trimmed of whitespaces and extraneous slashes at the end.
        /// </summary>
        public string TemplateDirectory {
            get {
                return _TemplateDirectory;
            }
            set {
                string val = value;
                if (value != null) {
                    val = value.Trim().TrimEnd('/', '\\');
                }
                _TemplateDirectory = val;

                if (themeConfigurationsCache != null)
                    themeConfigurationsCache.Clear();
            }
        }
        private string _TemplateDirectory;

        /// <summary>
        /// The theme to use when finding template.
        /// </summary>
        public string Theme {
            get { return _Theme; }
            set {
                if (value == null || value.Trim().Length == 0) {
                    throw new InvalidOperationException("Theme must be set to a valid non-null and non-empty value.");
                }
                _Theme = value;
            }
        }
        private string _Theme = DEFAULT_THEME_NAME;

        /// <summary>
        /// The template file extension, defaults to cshtml
        /// </summary>
        public string TemplateFileExtension {
            get {
                return _TemplateFileExtension;
            }
            set {
                if (value == null || value.Trim().Length == 0) {
                    _TemplateFileExtension = DEFAULT_TEMPLATE_FILE_EXTENSION;
                } else {
                    _TemplateFileExtension = value.Trim();
                }
            }
        }
        private string _TemplateFileExtension = DEFAULT_TEMPLATE_FILE_EXTENSION;

        /// <summary>
        /// The template file path generators that is sorted from highest priority
        /// to lowest priority. Template name generator is used by templater service
        /// to find appropriate template name given an object, render mode, and theme name.
        /// It can never be empty.
        /// </summary>
        public IEnumerable<ITemplateFilePathsGenerator> TemplateFilePathsGenerators {
            get {
                if (_TemplateFilePathsGenerators.Count == 0) {
                    return new List<ITemplateFilePathsGenerator>() {
                        defaultTemplateFilePathGenerator
                    };
                } else {
                    return _TemplateFilePathsGenerators.ToList();
                }
            }
        }
        private LinkedList<ITemplateFilePathsGenerator> _TemplateFilePathsGenerators;

        private Dictionary<string, ThemeConfiguration> themeConfigurationsCache;

        private DefaultTemplateFilePathsGenerator defaultTemplateFilePathGenerator;

        /// <summary>
        /// Create an instance of Configuration.
        /// <param name="templateDirectory">The root template directory to search for object templates in themes</param>
        /// </summary>
        public TemplaterConfiguration(string templateDirectory) {
            TemplateDirectory = templateDirectory;
            if (TemplateDirectory == null || !Directory.Exists(TemplateDirectory)) {
                throw new ArgumentException("Template directory must be specified and must exists.", "templateDirectory");
            }
            _TemplateFilePathsGenerators = new LinkedList<ITemplateFilePathsGenerator>();
            themeConfigurationsCache = new Dictionary<string, ThemeConfiguration>();
            defaultTemplateFilePathGenerator = new DefaultTemplateFilePathsGenerator();
        }

        /// <summary>
        /// Get theme configuration.
        /// </summary>
        /// <param name="themeName">The theme name</param>
        /// <returns>ThemeConfiguration object or null if the theme is not found</returns>
        public ThemeConfiguration GetThemeConfiguration(string themeName) {
            if (themeConfigurationsCache.ContainsKey(themeName)) {
                return themeConfigurationsCache[themeName];
            }

            var themePath = Path.Combine(TemplateDirectory, themeName);
            if (Directory.Exists(themePath)) {
                var themeConfigPath = Path.Combine(themePath, THEME_CONFIGURATION_FILE_NAME);
                var themeConfig = new ThemeConfiguration() {
                    Name = themeName
                };
                if (File.Exists(themeConfigPath)) {
                    var themeConfigJson = File.ReadAllText(themeConfigPath);
                    var themeCfgJs = JObject.Parse(themeConfigJson);

                    themeConfig.DisplayName = themeCfgJs["displayName"] == null ? themeName : themeCfgJs["displayName"].ToString();
                    themeConfig.Description = themeCfgJs["description"]?.ToString();
                    themeConfig.Author = themeCfgJs["author"]?.ToString();
                    themeConfig.Version = themeCfgJs["version"]?.ToString();
                    var alternativesJson = themeCfgJs["alternatives"]?.ToString();
                    if (alternativesJson != null) {
                        themeConfig.Alternatives = JsonConvert.DeserializeObject<List<string>>(alternativesJson);
                    }
                }
                themeConfigurationsCache.Remove(themeName);
                themeConfigurationsCache.Add(themeName, themeConfig);
                return themeConfig;
            } else {
                return null;
            }
        }

        /// <summary>
        /// Get all theme names in template directory.
        /// </summary>
        /// <returns>An array of theme names</returns>
        public IEnumerable<string> GetAllThemeNames() {
            var result = new List<string>();
            foreach (var themeDir in Directory.EnumerateDirectories(TemplateDirectory)) {
                result.Add(
                    themeDir.Substring(themeDir.LastIndexOf(Path.DirectorySeparatorChar) + 1)
                );
            }
            return result;
        }

        /// <summary>
        /// Get all theme configurations.
        /// </summary>
        /// <returns>An array of theme configurations</returns>
        public IEnumerable<ThemeConfiguration> GetAllThemeConfigurations() {
            var result = new List<ThemeConfiguration>();
            foreach (var thn in GetAllThemeNames()) {
                var thc = GetThemeConfiguration(thn);
                if (thc != null) {
                    result.Add(thc);
                }
            }
            return result;
        }

        /// <summary>
        /// Add a template file paths generator to this configuration.
        /// </summary>
        /// <param name="tfpGenerator">The template file paths generator object</param>
        /// <param name="priority">The priority, lower number means higher priority</param>
        public void AddTemplateFilePathsGenerator(ITemplateFilePathsGenerator tfpGenerator, int priority) {
            var existing = _TemplateFilePathsGenerators.Where(tng => tng == tfpGenerator).FirstOrDefault();
            if (existing != null) {
                _TemplateFilePathsGenerators.Remove(existing);
            }

            if (priority < 0) {
                _TemplateFilePathsGenerators.AddFirst(tfpGenerator);
            } else if (priority >= _TemplateFilePathsGenerators.Count) {
                _TemplateFilePathsGenerators.AddLast(tfpGenerator);
            } else {
                _TemplateFilePathsGenerators.AddBefore(
                    _TemplateFilePathsGenerators.Find(_TemplateFilePathsGenerators.ElementAt(priority)),
                    tfpGenerator
                );
            }
        }

        /// <summary>
        /// Remove a template file paths generator object from this configuration.
        /// </summary>
        /// <param name="tfpGenerator">The template file paths generator object</param>
        public void RemoveTemplateFilePathsGenerator(ITemplateFilePathsGenerator tfpGenerator) {
            _TemplateFilePathsGenerators.Remove(tfpGenerator);
        }

        /// <summary>
        /// Remove all template file paths generator of a specific type from this configuration.
        /// </summary>
        /// <param name="tfpGeneratorType">The template file paths generator type</param>
        public void RemoveTemplateFilePathsGenerator(Type tfpGeneratorType) {
            var rems = _TemplateFilePathsGenerators.Where(tng => tng.GetType() == tfpGeneratorType).ToList();
            foreach (var r in rems) {
                _TemplateFilePathsGenerators.Remove(r);
            }
        }
    }
}
