﻿using ObjectTemplater.Infrastructure.Exts;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ObjectTemplater.Models {
    internal class TypeTemplatePathsBuilder {
        public Type TheType { get; private set; }

        private List<Type> meAndMyBaseTypes;
        private List<TypeTemplateNode> rootNodes;

        public TypeTemplatePathsBuilder(Type type) {
            TheType = type;

            meAndMyBaseTypes = new List<Type>() { type };
            meAndMyBaseTypes.AddRange(GetBaseTypes(type));

            rootNodes = new List<TypeTemplateNode>();
            BuildNodesMap();
        }

        public IEnumerable<string> GenerateTemplatePaths() {
            var results = new List<string>();
            var buildingResults = new List<string>();
            foreach (var n in rootNodes) {
                BuildTemplatePaths(ref results, ref buildingResults, n);
            }
            return results;
        }

        private void BuildTemplatePaths(ref List<string> results, ref List<string> buildingResults, TypeTemplateNode currentNode) {
            var nbr = new List<string>(buildingResults);
            nbr.Add(currentNode.PathName);

            if (currentNode.Nexts.Count == 0) {
                results.Add(Path.Combine(nbr.ToArray()));
            } else {
                foreach (var nn in currentNode.Nexts) {
                    BuildTemplatePaths(ref results, ref nbr, nn);
                }
            }
        }

        private void BuildNodesMap() {
            foreach (var t in meAndMyBaseTypes) {
                var nodesOfNodes = new List<List<TypeTemplateNode>>();
                BuildNodes(ref nodesOfNodes, t, 0);

                for (var i = 0; i < nodesOfNodes.Count - 1; i++) {
                    var currentNodes = nodesOfNodes.ElementAt(i);
                    var nextNodes = nodesOfNodes.ElementAt(i + 1);

                    foreach (var cn in currentNodes) {
                        cn.Nexts.AddRange(nextNodes);
                    }
                }

                rootNodes.AddRange(nodesOfNodes.ElementAt(0));
            }
        }

        private void BuildNodes(
            ref List<List<TypeTemplateNode>> results, Type currentType, int level) {

            var thisLevelResult = new List<TypeTemplateNode>();
            foreach (var tplName in GetTypeTemplateNames(currentType)) {
                thisLevelResult.Add(
                    new TypeTemplateNode(ApplyPrefix(tplName, level))
                );
            }
            results.Add(thisLevelResult);

            foreach (var gta in currentType.GenericTypeArguments) {
                BuildNodes(ref results, gta, level + 1);
            }
        }

        private IEnumerable<Type> GetBaseTypes(Type type) {
            List<Type> bts = null;
            bts = new List<Type>();
            FindAllBaseTypes(ref bts, type, true);
            return bts;
        }

        private static void FindAllBaseTypes(ref List<Type> baseTypes, Type currentType, bool isTheType) {
            if (isTheType) {
                if (currentType.GenericTypeArguments.Count() >= 2) {
                    var iDictionaryType = GetIDictionaryType(
                            currentType.GenericTypeArguments.ElementAt(0),
                            currentType.GenericTypeArguments.ElementAt(1)
                        );
                    if (iDictionaryType.IsAssignableFrom(currentType) && iDictionaryType != currentType)
                        if (!baseTypes.Contains(iDictionaryType))
                            baseTypes.Add(iDictionaryType);
                }

                if (currentType.GenericTypeArguments.Count() >= 1) {
                    var iCollectionType = GetICollectionType(
                            currentType.GenericTypeArguments.ElementAt(0)
                        );
                    if (iCollectionType.IsAssignableFrom(currentType) && iCollectionType != currentType)
                        if (!baseTypes.Contains(iCollectionType))
                            baseTypes.Add(iCollectionType);

                    var iEnumerableType = GetIEnumerableType(
                            currentType.GenericTypeArguments.ElementAt(0)
                        );
                    if (iEnumerableType.IsAssignableFrom(currentType) && iEnumerableType != currentType)
                        if (!baseTypes.Contains(iEnumerableType))
                            baseTypes.Add(iEnumerableType);
                }
            }

            if (currentType.IsInterface) {
                if (!baseTypes.Contains(typeof(object)))
                    baseTypes.Add(typeof(object));
            } else if (currentType.BaseType != null) {
                if (!baseTypes.Contains(currentType.BaseType))
                    baseTypes.Add(currentType.BaseType);
                FindAllBaseTypes(ref baseTypes, currentType.BaseType, false);
            }
        }

        private static Type GetIEnumerableType(params Type[] pars) {
            return typeof(IEnumerable<>).MakeGenericType(pars);
        }
        private static Type GetICollectionType(params Type[] pars) {
            return typeof(ICollection<>).MakeGenericType(pars);
        }
        private static Type GetIDictionaryType(params Type[] pars) {
            return typeof(IDictionary<,>).MakeGenericType(pars);
        }

        private static string ApplyPrefix(string templateName, int prefix) {
            if (prefix != 0) {
                return string.Format("{0}.{1}", prefix, templateName);
            } else {
                return templateName;
            }
        }

        private static IEnumerable<string> GetTypeTemplateNames(Type type) {
            var tplNameReal = type.ToTemplateName();
            var tplNameAlias = null as string;
            if (aliases.TryGetValue(tplNameReal, out tplNameAlias))
                yield return tplNameAlias;
            yield return tplNameReal;
        }

        private static Dictionary<string, string> aliases = new Dictionary<string, string>() {
            [typeof(object).ToTemplateName()] = "object",
            [typeof(string).ToTemplateName()] = "string",
            [typeof(bool).ToTemplateName()] = "bool",
            [typeof(int).ToTemplateName()] = "int",
            [typeof(long).ToTemplateName()] = "long",
            [typeof(float).ToTemplateName()] = "float",
            [typeof(double).ToTemplateName()] = "double",
            [typeof(decimal).ToTemplateName()] = "decimal",
            [typeof(DateTime).ToTemplateName()] = "DateTime",
            [typeof(Tuple<>).ToTemplateName()] = "Tuple",
            [typeof(Nullable<>).ToTemplateName()] = "Nullable",
            ["System.Collections.Generic.LinkedList"] = "LinkedList",
            ["System.Collections.Generic.List"] = "List",
            ["System.Collections.Generic.IList"] = "IList",
            ["System.Collections.Generic.Dictionary"] = "Dictionary",
            ["System.Collections.Generic.IDictionary"] = "IDictionary",
            ["System.Collections.Generic.ICollection"] = "ICollection",
            ["System.Collections.Generic.IEnumerable"] = "IEnumerable",
        };
    }

    internal class TypeTemplateNode {
        public string PathName { get; private set; }
        public List<TypeTemplateNode> Nexts { get; private set; }

        public TypeTemplateNode(string name) {
            PathName = name;
            Nexts = new List<TypeTemplateNode>();
        }
    }
}
