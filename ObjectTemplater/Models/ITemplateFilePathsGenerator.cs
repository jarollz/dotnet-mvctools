﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ObjectTemplater.Models {
    /// <summary>
    /// This is the generic interface for all classes that can yield one or more template names
    /// given an object, render mode, and theme name
    /// </summary>
    public interface ITemplateFilePathsGenerator {
        /// <summary>
        /// Generate template file paths
        /// </summary>
        /// <param name="obj">the object whose template name is being generated</param>
        /// <param name="renderMode">the render mode</param>
        /// <param name="themeNames">the list of possible theme name to use sorted from highest priority to lowest</param>
        /// <param name="rootTemplateDirectory">the template root directory</param>
        /// <param name="templateFileExtension">the template file extension to use</param>
        IEnumerable<string> GenerateTemplateFilePaths(
            object obj,
            string renderMode,
            IEnumerable<string> themeNames, 
            string rootTemplateDirectory,
            string templateFileExtension);

        /// <summary>
        /// Generate template file paths
        /// </summary>
        /// <param name="type">the type whose template name is being generated</param>
        /// <param name="renderMode">the render mode</param>
        /// <param name="themeNames">the list of possible theme name to use sorted from highest priority to lowest</param>
        /// <param name="rootTemplateDirectory">the template root directory</param>
        /// <param name="templateFileExtension">the template file extension to use</param>
        IEnumerable<string> GenerateTemplateFilePaths(
            Type type,
            string renderMode,
            IEnumerable<string> themeNames,
            string rootTemplateDirectory,
            string templateFileExtension);
    }
}
