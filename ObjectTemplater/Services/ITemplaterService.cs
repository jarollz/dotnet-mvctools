﻿using System.Collections.Generic;
using ObjectTemplater.Models;
using System;

namespace ObjectTemplater.Services {
    /// <summary>
    /// This is the interface for all classes that can find suitable template for an object
    /// </summary>
    public interface ITemplaterService {
        TemplaterConfiguration Config { get; }

        /// <summary>
        /// Get the full real path of the template cshtml to use when rendering an object
        /// according to the render mode and theme name
        /// </summary>
        /// <param name="obj">The object</param>
        /// <param name="renderMode">The render mode. If unspecified or null, the system will assume render mode 'view' is used.</param>
        /// <param name="themeName">The theme name. If unspecified or null, the system will select the default theme of the render mode according to configuration.</param>
        /// <returns>A template file path that exists in file system. If not found, will throw exception</returns>
        IEnumerable<string> GetAllPossibleTemplateFilePaths(object obj, string renderMode = null, string themeName = null);

        /// <summary>
        /// Get all possible template file paths to consider for an object using specific render mode and theme name
        /// </summary>
        /// <param name="obj">The object</param>
        /// <param name="renderMode">The render mode. If unspecified or null, the system will assume render mode 'view' is used.</param>
        /// <param name="themeName">The theme name. If unspecified or null, the system will select the default theme of the render mode according to configuration.</param>
        /// <returns>A list of template file paths</returns>
        string GetTemplateFilePath(object obj, string renderMode = null, string themeName = null);

        /// <summary>
        /// Get all possible template file paths to consider for an object using specific render mode and theme name
        /// </summary>
        /// <param name="obj">The object</param>
        /// <param name="renderMode">The render mode. If unspecified or null, the system will assume render mode 'view' is used.</param>
        /// <param name="themeName">The theme name. If unspecified or null, the system will select the default theme of the render mode according to configuration.</param>
        /// <returns>A list of template file paths</returns>
        string GetTemplateFilePathOrNullIfNotFound(object obj, string renderMode = null, string themeName = null);

        /// <summary>
        /// Get the full real path of the template cshtml to use when rendering a type
        /// according to the render mode and theme name
        /// </summary>
        /// <param name="type">The object type</param>
        /// <param name="renderMode">The render mode. If unspecified or null, the system will assume render mode 'view' is used.</param>
        /// <param name="themeName">The theme name. If unspecified or null, the system will select the default theme of the render mode according to configuration.</param>
        /// <returns>A template file path that exists in file system. If not found, will throw exception</returns>
        IEnumerable<string> GetAllPossibleTemplateFilePaths(Type type, string renderMode = null, string themeName = null);

        /// <summary>
        /// Get all possible template file paths to consider for a type using specific render mode and theme name
        /// </summary>
        /// <param name="type">The object type</param>
        /// <param name="renderMode">The render mode. If unspecified or null, the system will assume render mode 'view' is used.</param>
        /// <param name="themeName">The theme name. If unspecified or null, the system will select the default theme of the render mode according to configuration.</param>
        /// <returns>A list of template file paths</returns>
        string GetTemplateFilePath(Type type, string renderMode = null, string themeName = null);

        /// <summary>
        /// Get all possible template file paths to consider for a type using specific render mode and theme name
        /// </summary>
        /// <param name="type">The object type</param>
        /// <param name="renderMode">The render mode. If unspecified or null, the system will assume render mode 'view' is used.</param>
        /// <param name="themeName">The theme name. If unspecified or null, the system will select the default theme of the render mode according to configuration.</param>
        /// <returns>A list of template file paths</returns>
        string GetTemplateFilePathOrNullIfNotFound(Type type, string renderMode = null, string themeName = null);

        /// <summary>
        /// Get theme name and its alternatives ordered by priority.
        /// </summary>
        /// <param name="themeName">The theme name to check</param>
        /// <returns>theme names</returns>
        IEnumerable<string> GetValidThemeAlternatives(string themeName);

        /// <summary>
        /// Get a valid render mode string
        /// </summary>
        /// <param name="requestedRenderMode">render mode</param>
        /// <returns>valid render mode</returns>
        string GetValidRenderMode(string requestedRenderMode);
    }
}