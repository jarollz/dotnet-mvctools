﻿using ObjectTemplater.Infrastructure.Exts;
using ObjectTemplater.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ObjectTemplater.Services {
    /// <summary>
    /// This is the class that will be the entry point for the main task of this library i.e.
    /// rendering object according to user specified rendering mode, and using that information,
    /// find the most suitable template for it.
    /// </summary>
    public class TemplaterService : ITemplaterService {

        public TemplaterConfiguration Config {
            get {
                return _Config;
            }
        }
        private TemplaterConfiguration _Config;

        /// <summary>
        /// Create an instance of TemplaterService
        /// </summary>
        public TemplaterService(TemplaterConfiguration config) {
            if (config == null) {
                throw new ArgumentException("Templater configuration can't be null.", "config");
            }
            _Config = config;
        }

        /// <summary>
        /// Get the full real path of the template cshtml to use when rendering an object
        /// according to the render mode and theme name
        /// </summary>
        /// <param name="obj">The object</param>
        /// <param name="renderMode">The render mode. If unspecified or null, the system will assume render mode 'view' is used.</param>
        /// <param name="themeName">The theme name. If unspecified or null, the system will select the default theme of the render mode according to configuration.</param>
        /// <returns>A template file path that exists in file system. If not found, will throw exception</returns>
        public string GetTemplateFilePath(object obj, string renderMode = null, string themeName = null) {
            string foundPath = GetTemplateFilePathOrNullIfNotFound(obj, renderMode, themeName);
            if (foundPath == null) {
                var searchedPaths = new List<string>();
                var sps = GetAllPossibleTemplateFilePaths(obj, renderMode, themeName);
                foreach (var sp in sps) {
                    searchedPaths.Add(sp.Replace(_Config.TemplateDirectory, "<template-root>"));
                }
                throw new FileNotFoundException(string.Format("Object templater failed to find template file for the given object (toString: {0}, type: {1}).\nSearched paths:\n{2}",
                    obj.ToString(), obj.GetType().FullName, string.Join(";\n", searchedPaths)));
            }
            return foundPath;
        }

        /// <summary>
        /// Get the full real path of the template cshtml to use when rendering an object
        /// according to the render mode and theme name
        /// </summary>
        /// <param name="obj">The object</param>
        /// <param name="renderMode">The render mode. If unspecified or null, the system will assume render mode 'view' is used.</param>
        /// <param name="themeName">The theme name. If unspecified or null, the system will select the default theme of the render mode according to configuration.</param>
        /// <returns>A template file path that exists in file system. If not found, will throw exception</returns>
        public string GetTemplateFilePathOrNullIfNotFound(object obj, string renderMode = null, string themeName = null) {
            if (obj == null) throw new ArgumentException("Can't find any template for null object.", "obj");

            string foundPath = null;
            var rm = GetValidRenderMode(renderMode);
            var tns = GetValidThemeAlternatives(themeName);

            foreach (var tng in _Config.TemplateFilePathsGenerators) {
                foreach (var templateFilePath in tng.GenerateTemplateFilePaths(
                    obj, rm, tns, _Config.TemplateDirectory, _Config.TemplateFileExtension)) {

                    if (File.Exists(templateFilePath)) {
                        foundPath = templateFilePath;
                    }
                    if (foundPath != null) break;
                }
                if (foundPath != null) break;
            }

            return foundPath;
        }

        /// <summary>
        /// Get the full real path of the template cshtml to use when rendering a type
        /// according to the render mode and theme name
        /// </summary>
        /// <param name="type">The object type</param>
        /// <param name="renderMode">The render mode. If unspecified or null, the system will assume render mode 'view' is used.</param>
        /// <param name="themeName">The theme name. If unspecified or null, the system will select the default theme of the render mode according to configuration.</param>
        /// <returns>A template file path that exists in file system. If not found, will throw exception</returns>
        public string GetTemplateFilePath(Type type, string renderMode = null, string themeName = null) {
            string foundPath = GetTemplateFilePathOrNullIfNotFound(type, renderMode, themeName);
            if (foundPath == null) {
                var searchedPaths = new List<string>();
                var sps = GetAllPossibleTemplateFilePaths(type, renderMode, themeName);
                foreach (var sp in sps) {
                    searchedPaths.Add(sp.Replace(_Config.TemplateDirectory, "<template-root>"));
                }
                throw new FileNotFoundException(string.Format("Object templater failed to find template file for the type {0}.\nSearched paths:\n{1}",
                    type.FullName, string.Join(";\n", searchedPaths)));
            }

            return foundPath;
        }

        /// <summary>
        /// Get the full real path of the template cshtml to use when rendering a type
        /// according to the render mode and theme name
        /// </summary>
        /// <param name="type">The object type</param>
        /// <param name="renderMode">The render mode. If unspecified or null, the system will assume render mode 'view' is used.</param>
        /// <param name="themeName">The theme name. If unspecified or null, the system will select the default theme of the render mode according to configuration.</param>
        /// <returns>A template file path that exists in file system. If not found, will throw exception</returns>
        public string GetTemplateFilePathOrNullIfNotFound(Type type, string renderMode = null, string themeName = null) {
            if (type == null) throw new ArgumentException("Can't find any template for null type.", "type");

            string foundPath = null;
            var rm = GetValidRenderMode(renderMode);
            var tns = GetValidThemeAlternatives(themeName);

            foreach (var tng in _Config.TemplateFilePathsGenerators) {
                foreach (var templateFilePath in tng.GenerateTemplateFilePaths(
                    type, rm, tns, _Config.TemplateDirectory, _Config.TemplateFileExtension)) {

                    if (File.Exists(templateFilePath)) {
                        foundPath = templateFilePath;
                    }
                    if (foundPath != null) break;
                }
                if (foundPath != null) break;
            }
            
            return foundPath;
        }

        /// <summary>
        /// Get all possible template file paths to consider for an object using specific render mode and theme name
        /// </summary>
        /// <param name="obj">The object</param>
        /// <param name="renderMode">The render mode. If unspecified or null, the system will assume render mode 'view' is used.</param>
        /// <param name="themeName">The theme name. If unspecified or null, the system will select the default theme of the render mode according to configuration.</param>
        /// <returns>A list of template file paths</returns>
        public IEnumerable<string> GetAllPossibleTemplateFilePaths(object obj, string renderMode = null, string themeName = null) {
            if (obj == null) throw new ArgumentException("Can't find any template for null object.", "obj");

            var result = new List<string>();
            var rm = GetValidRenderMode(renderMode);
            var tns = GetValidThemeAlternatives(themeName);
            
            foreach (var tng in _Config.TemplateFilePathsGenerators) {
                foreach (var templateFilePath in tng.GenerateTemplateFilePaths(
                    obj, rm, tns, _Config.TemplateDirectory, _Config.TemplateFileExtension)) {

                    result.Add(templateFilePath);
                }
            }

            return result;
        }

        /// <summary>
        /// Get all possible template file paths to consider for a type using specific render mode and theme name
        /// </summary>
        /// <param name="type">The object type</param>
        /// <param name="renderMode">The render mode. If unspecified or null, the system will assume render mode 'view' is used.</param>
        /// <param name="themeName">The theme name. If unspecified or null, the system will select the default theme of the render mode according to configuration.</param>
        /// <returns>A list of template file paths</returns>
        public IEnumerable<string> GetAllPossibleTemplateFilePaths(Type type, string renderMode = null, string themeName = null) {
            if (type == null) throw new ArgumentException("Can't find any template for null type.", "type");

            var result = new List<string>();
            var rm = GetValidRenderMode(renderMode);
            var tns = GetValidThemeAlternatives(themeName);

            foreach (var tng in _Config.TemplateFilePathsGenerators) {
                foreach (var templateFilePath in tng.GenerateTemplateFilePaths(
                    type, rm, tns, _Config.TemplateDirectory, _Config.TemplateFileExtension)) {

                    result.Add(templateFilePath);
                }
            }

            return result;
        }

        /// <summary>
        /// Get theme name and its alternatives ordered by priority.
        /// </summary>
        /// <param name="themeName">The theme name to check</param>
        /// <returns>theme names</returns>
        public IEnumerable<string> GetValidThemeAlternatives(string themeName) {
            string tn = themeName;
            if (themeName != null) {
                tn = themeName.EnsureStringIsTrimmedAndLowerCase();
            } else {
                tn = _Config.Theme;
            }

            yield return tn;

            var themeCfg = _Config.GetThemeConfiguration(tn);
            foreach (var alt in themeCfg.Alternatives) {
                yield return alt;
            }
        }

        /// <summary>
        /// Get a valid render mode string
        /// </summary>
        /// <param name="requestedRenderMode">render mode</param>
        /// <returns>valid render mode</returns>
        public string GetValidRenderMode(string requestedRenderMode) {
            string rm = requestedRenderMode;
            if (requestedRenderMode != null) {
                rm = requestedRenderMode.EnsureStringIsTrimmedAndLowerCase();
            } else {
                rm = TemplaterConfiguration.DEFAULT_RENDER_MODE;
            }
            return rm;
        }

        
    }
}
