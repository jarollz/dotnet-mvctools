﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace WebTester.Models {
    public class TestForm {
        [Display(Name ="String Data")]
        [Required]
        public string SomeString { get; set; }
        [Display(Name = "Int? Data")]
        public int? SomeNullableInt { get; set; }
        [Display(Name = "DateTime Data")]
        public DateTime SomeDateTime { get; set; }
        [Display(Name = "List<string> Data")]
        public ICollection<string> SomeListOfString { get; set; }
        [Display(Name = "Dictionary<string,int> Data")]
        public IDictionary<string, int> SomeDictionaryOfStringInt { get; set; }
        [Display(Name = "TestFormChild Data")]
        public TestFormChild SomeTestFormChild { get; set; }
    }
}