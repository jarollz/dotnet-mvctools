﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace WebTester.Models {
    public class TestFormChild {
        [Display(Name ="Float Data")]
        [Required]
        public float SomeFloat { get; set; }
        [Display(Name = "DateTime? Data")]
        public DateTime? SomeNullableDateTime { get; set; }
        [Display(Name = "List<int> Data")]
        public ICollection<int> SomeListOfInt { get; set; }
    }
}