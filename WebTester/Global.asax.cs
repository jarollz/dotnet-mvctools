﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Hosting;
using System.Web.Mvc;
using System.Web.Routing;
using MvcActionIpFilter.Configuration;
using System.Web.Configuration;

namespace WebTester {
    public class MvcApplication : System.Web.HttpApplication {
        protected void Application_Start() {
            ObjectRenderer.ThemeService.UseThemedRazorViewEngine(true);

            AreaRegistration.RegisterAllAreas();
            RouteConfig.RegisterRoutes(RouteTable.Routes);
        }
    }
}
