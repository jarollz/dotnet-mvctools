﻿using MvcActionIpFilter;
using ObjectRenderer;
using ObjectRenderer.Models;
using ObjectTemplater.Models;
using ObjectTemplater.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Hosting;
using System.Web.Mvc;
using WebTester.Models;

namespace WebTester.Controllers {
    public class HomeController : Controller {
        protected override void OnActionExecuting(ActionExecutingContext filterContext) {
            ThemeService.Theme = "default";
            base.OnActionExecuting(filterContext);
        }
        // GET: Home
        public ActionResult Index() {
            return View();
        }

        public ActionResult ObjectTemplaterTest() {
            var templateRootPath = Server.MapPath("~/Views/_ObjectTemplater");
            var defaultConfig = new TemplaterConfiguration(templateRootPath);
            defaultConfig.AddTemplateFilePathsGenerator(
                new ErrorTemplateFilePathsGenerator(), 0
            );
            defaultConfig.AddTemplateFilePathsGenerator(
                new DefaultTemplateFilePathsGenerator(), 1
            );
            var testConfig1 = new TemplaterConfiguration(templateRootPath);
            testConfig1.Theme = "theme_one";
            var testConfig2 = new TemplaterConfiguration(templateRootPath);
            testConfig2.Theme = "theme_two";

            var defaultTemplater = new TemplaterService(defaultConfig);
            var testTemplater1 = new TemplaterService(testConfig1);
            var testTemplater2 = new TemplaterService(testConfig2);

            var testObjects = new List<object>() {
                "Some string",
                true,
                1,
                1L,
                1.0f,
                1.0d,
                1.0m,
                DateTime.Now,
                new DateTime?(DateTime.Now),
                new long?(1L),
                new Shape(),
                new Rectangle(),
                new Square(),
                new Ellipse(),
                new Circle(),
                new Exception("Some exception"),
                new HttpException(404, "Not Found"),
                new HttpException(500, "Internal Server Error"),
                Tuple.Create(1,"tuple",false),
                new Dictionary<string, int>(),
                new List<float?>(),
            };

            ViewBag.DefaultTemplater = defaultTemplater;
            ViewBag.TestTemplater1 = testTemplater1;
            ViewBag.TestTemplater2 = testTemplater2;
            ViewBag.Templaters = new TemplaterService[] {
                defaultTemplater,
                testTemplater1,
                testTemplater2
            };
            ViewBag.TestObjects = testObjects;

            return View();
        }

        public ActionResult PartialViewTest() {
            ViewBag.AnotherAbsolutePath = HostingEnvironment.MapPath("~/Views/_Partials/_Sample.cshtml");

            return View();
        }

        [HttpGet]
        public ActionResult FormModelTest() {
            var testForm = new TestForm() {
                SomeString = "test form",
                SomeNullableInt = 99,
                SomeDateTime = new DateTime(2000, 1, 1, 12, 0, 0),
                SomeListOfString = new List<string>() {
                    "test form list element 1",
                    "test form list element 2",
                    "test form list element 3"
                },
                SomeDictionaryOfStringInt = new Dictionary<string, int>() {
                    { "ronaldo", 7 },
                    { "pogba", 6 },
                    { "zlatan", 9 }
                },
                SomeTestFormChild = new TestFormChild() {
                    SomeFloat = 3.14f,
                    SomeNullableDateTime = new DateTime(2016, 6, 10, 10, 6, 0),
                    SomeListOfInt = new List<int>() {
                        5,4,3,2,1
                    }
                }
            };
            ViewBag.DetachedModel = new TestFormChild() {
                SomeFloat = 1.0f,
                SomeNullableDateTime = DateTime.Now,
                SomeListOfInt = new List<int>() {
                    11,22,33,44,55
                }
            };
            return View(testForm);
        }

        [HttpPost]
        public ActionResult FormModelTest(TestForm mdl) {
            var modelIsValid = ModelState.IsValid;
            ViewBag.ModelIsValid = modelIsValid;
            ViewBag.DetachedModel = new TestFormChild() {
                SomeFloat = 2.0f,
                SomeNullableDateTime = DateTime.Now,
                SomeListOfInt = new List<int>() {
                    66,77,88,99,100
                }
            };
            return View(mdl);
        }

        public ActionResult RenderSectionTest() {
            return View();
        }

        public ActionResult WebLoggerExceptionTest() {
            throw new Exception(string.Format("Web logger exception test done at {0}", DateTime.Now.ToString("yyyy-MMM-dd HH:mm:ss")));
        }

        [ActionIpFilter("test2")]
        public ActionResult IpFilterCheck() {
            return Content("Checked", "text/plain");
        }

        public ActionResult ThemedRazorViewEngineTest() {
            return View();
        }
    }
}