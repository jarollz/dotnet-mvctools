﻿using log4net;
using log4net.Config;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Hosting;

namespace WebLogger {
    public static class LogService {
        private static bool isInitialized = false;
        private static object initializationLock = new object();
        static LogService() {
            lock (initializationLock) {
                if (!isInitialized) {
                    var configAbsolutePath = HostingEnvironment.MapPath(DEFAULT_CONFIG_FILE_RELATIVE_PATH);
                    var configFileExists = false;
                    if (!File.Exists(configAbsolutePath)) {
                        try {
                            File.WriteAllText(configAbsolutePath, DEFAULT_CONFIG);
                            configFileExists = true;
                        } catch (Exception) {
                            // can't create config file.
                        }
                    } else {
                        configFileExists = true;
                    }
                    if (configFileExists) {
                        var cfgFinfo = new FileInfo(configAbsolutePath);
                        XmlConfigurator.ConfigureAndWatch(cfgFinfo);
                    } else {
                        XmlConfigurator.Configure(GenerateStreamFromString(DEFAULT_CONFIG));
                    }

                    isInitialized = true;
                }
            }
        }
        
        public static ILog GetLogger(string n) {
            return LogManager.GetLogger(n);
        }

        public static ILog GetLogger(Type t) {
            return LogManager.GetLogger(t);
        }
        
        public const string DEFAULT_CONFIG_FILE_RELATIVE_PATH = "~/log4net.config";

        public const string DEFAULT_CONFIG = @"<?xml version=""1.0"" encoding=""utf-8"" ?>
<log4net>
    <appender name=""ApplicationAppender"" type=""log4net.Appender.RollingFileAppender"">
        <param name=""File"" value=""App_Data/Logs/app.log"" />
        <param name=""PreserveLogFileNameExtension"" value=""True"" />
        <param name=""StaticLogFileName"" value=""True"" />
        <param name=""RollingStyle"" value=""Composite"" />
        <param name=""DatePattern"" value="".yyyy-MM-dd"" />
        <param name=""MaximumFileSize"" value=""1MB"" />
        <param name=""MaxSizeRollBackups"" value=""-1"" />
        <param name=""CountDirection"" value=""1"" />
        <layout type=""log4net.Layout.PatternLayout"">
            <conversionPattern value=""#######################%newline%date [%level] on %logger thread:%thread%newlineFrom: %class at %location%newlineMessage: %message%newlineNDC: %ndc%newlineStacktrace: %stacktracedetail{10}%newline"" />
        </layout>
        <filter type=""log4net.Filter.LevelRangeFilter"">
            <levelMin value=""INFO"" />
            <levelMax value=""FATAL"" />
        </filter>
    </appender>

    <appender name=""ApplicationMessagesAppender"" type=""log4net.Appender.RollingFileAppender"">
        <param name=""File"" value=""App_Data/Logs/app-messages.log"" />
        <param name=""PreserveLogFileNameExtension"" value=""True"" />
        <param name=""StaticLogFileName"" value=""True"" />
        <param name=""RollingStyle"" value=""Composite"" />
        <param name=""DatePattern"" value="".yyyy-MM-dd"" />
        <param name=""MaximumFileSize"" value=""1MB"" />
        <param name=""MaxSizeRollBackups"" value=""-1"" />
        <param name=""CountDirection"" value=""1"" />
        <layout type=""log4net.Layout.PatternLayout"">
            <conversionPattern value=""%date: %message%newline"" />
        </layout>
        <filter type=""log4net.Filter.LevelRangeFilter"">
            <levelMin value=""INFO"" />
            <levelMax value=""FATAL"" />
        </filter>
    </appender>

    <appender name=""DebugOnlyAppender"" type=""log4net.Appender.RollingFileAppender"">
        <param name=""File"" value=""App_Data/Logs/debug.log"" />
        <param name=""PreserveLogFileNameExtension"" value=""True"" />
        <param name=""StaticLogFileName"" value=""True"" />
        <param name=""RollingStyle"" value=""Composite"" />
        <param name=""DatePattern"" value="".yyyy-MM-dd"" />
        <param name=""MaximumFileSize"" value=""1MB"" />
        <param name=""MaxSizeRollBackups"" value=""-1"" />
        <param name=""CountDirection"" value=""1"" />
        <layout type=""log4net.Layout.PatternLayout"">
            <conversionPattern value=""#######################%newline%date [%level] on %logger thread:%thread%newlineFrom: %class at %location%newlineMessage: %message%newlineNDC: %ndc%newlineStacktrace: %stacktracedetail{10}%newline"" />
        </layout>
        <filter type=""log4net.Filter.LevelRangeFilter"">
            <levelMin value=""DEBUG"" />
            <levelMax value=""DEBUG"" />
        </filter>
    </appender>

    <appender name=""DebugOnlyMessagesAppender"" type=""log4net.Appender.RollingFileAppender"">
        <param name=""File"" value=""App_Data/Logs/debug-messages.log"" />
        <param name=""PreserveLogFileNameExtension"" value=""True"" />
        <param name=""StaticLogFileName"" value=""True"" />
        <param name=""RollingStyle"" value=""Composite"" />
        <param name=""DatePattern"" value="".yyyy-MM-dd"" />
        <param name=""MaximumFileSize"" value=""1MB"" />
        <param name=""MaxSizeRollBackups"" value=""-1"" />
        <param name=""CountDirection"" value=""1"" />
        <layout type=""log4net.Layout.PatternLayout"">
            <conversionPattern value=""%date: %message%newline"" />
        </layout>
        <filter type=""log4net.Filter.LevelRangeFilter"">
            <levelMin value=""DEBUG"" />
            <levelMax value=""DEBUG"" />
        </filter>
    </appender>

    <appender name=""InfoOnlyAppender"" type=""log4net.Appender.RollingFileAppender"">
        <param name=""File"" value=""App_Data/Logs/info.log"" />
        <param name=""PreserveLogFileNameExtension"" value=""True"" />
        <param name=""StaticLogFileName"" value=""True"" />
        <param name=""RollingStyle"" value=""Composite"" />
        <param name=""DatePattern"" value="".yyyy-MM-dd"" />
        <param name=""MaximumFileSize"" value=""1MB"" />
        <param name=""MaxSizeRollBackups"" value=""-1"" />
        <param name=""CountDirection"" value=""1"" />
        <layout type=""log4net.Layout.PatternLayout"">
            <conversionPattern value=""#######################%newline%date [%level] on %logger thread:%thread%newlineFrom: %class at %location%newlineMessage: %message%newlineNDC: %ndc%newlineStacktrace: %stacktracedetail{10}%newline"" />
        </layout>
        <filter type=""log4net.Filter.LevelRangeFilter"">
            <levelMin value=""INFO"" />
            <levelMax value=""INFO"" />
        </filter>
    </appender>

    <appender name=""InfoOnlyMessagesAppender"" type=""log4net.Appender.RollingFileAppender"">
        <param name=""File"" value=""App_Data/Logs/info-messages.log"" />
        <param name=""PreserveLogFileNameExtension"" value=""True"" />
        <param name=""StaticLogFileName"" value=""True"" />
        <param name=""RollingStyle"" value=""Composite"" />
        <param name=""DatePattern"" value="".yyyy-MM-dd"" />
        <param name=""MaximumFileSize"" value=""1MB"" />
        <param name=""MaxSizeRollBackups"" value=""-1"" />
        <param name=""CountDirection"" value=""1"" />
        <layout type=""log4net.Layout.PatternLayout"">
            <conversionPattern value=""%date: %message%newline"" />
        </layout>
        <filter type=""log4net.Filter.LevelRangeFilter"">
            <levelMin value=""INFO"" />
            <levelMax value=""INFO"" />
        </filter>
    </appender>

    <appender name=""WarnOnlyAppender"" type=""log4net.Appender.RollingFileAppender"">
        <param name=""File"" value=""App_Data/Logs/warn.log"" />
        <param name=""PreserveLogFileNameExtension"" value=""True"" />
        <param name=""StaticLogFileName"" value=""True"" />
        <param name=""RollingStyle"" value=""Composite"" />
        <param name=""DatePattern"" value="".yyyy-MM-dd"" />
        <param name=""MaximumFileSize"" value=""1MB"" />
        <param name=""MaxSizeRollBackups"" value=""-1"" />
        <param name=""CountDirection"" value=""1"" />
        <layout type=""log4net.Layout.PatternLayout"">
            <conversionPattern value=""#######################%newline%date [%level] on %logger thread:%thread%newlineFrom: %class at %location%newlineMessage: %message%newlineNDC: %ndc%newlineStacktrace: %stacktracedetail{10}%newline"" />
        </layout>
        <filter type=""log4net.Filter.LevelRangeFilter"">
            <levelMin value=""WARN"" />
            <levelMax value=""WARN"" />
        </filter>
    </appender>

    <appender name=""WarnOnlyMessagesAppender"" type=""log4net.Appender.RollingFileAppender"">
        <param name=""File"" value=""App_Data/Logs/warn-messages.log"" />
        <param name=""PreserveLogFileNameExtension"" value=""True"" />
        <param name=""StaticLogFileName"" value=""True"" />
        <param name=""RollingStyle"" value=""Composite"" />
        <param name=""DatePattern"" value="".yyyy-MM-dd"" />
        <param name=""MaximumFileSize"" value=""1MB"" />
        <param name=""MaxSizeRollBackups"" value=""-1"" />
        <param name=""CountDirection"" value=""1"" />
        <layout type=""log4net.Layout.PatternLayout"">
            <conversionPattern value=""%date: %message%newline"" />
        </layout>
        <filter type=""log4net.Filter.LevelRangeFilter"">
            <levelMin value=""WARN"" />
            <levelMax value=""WARN"" />
        </filter>
    </appender>

    <appender name=""ErrorOnlyAppender"" type=""log4net.Appender.RollingFileAppender"">
        <param name=""File"" value=""App_Data/Logs/error.log"" />
        <param name=""PreserveLogFileNameExtension"" value=""True"" />
        <param name=""StaticLogFileName"" value=""True"" />
        <param name=""RollingStyle"" value=""Composite"" />
        <param name=""DatePattern"" value="".yyyy-MM-dd"" />
        <param name=""MaximumFileSize"" value=""1MB"" />
        <param name=""MaxSizeRollBackups"" value=""-1"" />
        <param name=""CountDirection"" value=""1"" />
        <layout type=""log4net.Layout.PatternLayout"">
            <conversionPattern value=""#######################%newline%date [%level] on %logger thread:%thread%newlineFrom: %class at %location%newlineMessage: %message%newlineNDC: %ndc%newlineStacktrace: %stacktracedetail{10}%newline"" />
        </layout>
        <filter type=""log4net.Filter.LevelRangeFilter"">
            <levelMin value=""ERROR"" />
            <levelMax value=""ERROR"" />
        </filter>
    </appender>

    <appender name=""ErrorOnlyMessagesAppender"" type=""log4net.Appender.RollingFileAppender"">
        <param name=""File"" value=""App_Data/Logs/error-messages.log"" />
        <param name=""PreserveLogFileNameExtension"" value=""True"" />
        <param name=""StaticLogFileName"" value=""True"" />
        <param name=""RollingStyle"" value=""Composite"" />
        <param name=""DatePattern"" value="".yyyy-MM-dd"" />
        <param name=""MaximumFileSize"" value=""1MB"" />
        <param name=""MaxSizeRollBackups"" value=""-1"" />
        <param name=""CountDirection"" value=""1"" />
        <layout type=""log4net.Layout.PatternLayout"">
            <conversionPattern value=""%date: %message%newline"" />
        </layout>
        <filter type=""log4net.Filter.LevelRangeFilter"">
            <levelMin value=""ERROR"" />
            <levelMax value=""ERROR"" />
        </filter>
    </appender>

    <appender name=""FatalOnlyAppender"" type=""log4net.Appender.RollingFileAppender"">
        <param name=""File"" value=""App_Data/Logs/fatal.log"" />
        <param name=""PreserveLogFileNameExtension"" value=""True"" />
        <param name=""StaticLogFileName"" value=""True"" />
        <param name=""RollingStyle"" value=""Composite"" />
        <param name=""DatePattern"" value="".yyyy-MM-dd"" />
        <param name=""MaximumFileSize"" value=""1MB"" />
        <param name=""MaxSizeRollBackups"" value=""-1"" />
        <param name=""CountDirection"" value=""1"" />
        <layout type=""log4net.Layout.PatternLayout"">
            <conversionPattern value=""#######################%newline%date [%level] on %logger thread:%thread%newlineFrom: %class at %location%newlineMessage: %message%newlineNDC: %ndc%newlineStacktrace: %stacktracedetail{10}%newline"" />
        </layout>
        <filter type=""log4net.Filter.LevelRangeFilter"">
            <levelMin value=""FATAL"" />
            <levelMax value=""FATAL"" />
        </filter>
    </appender>

    <appender name=""FatalOnlyMessagesAppender"" type=""log4net.Appender.RollingFileAppender"">
        <param name=""File"" value=""App_Data/Logs/fatal-messages.log"" />
        <param name=""PreserveLogFileNameExtension"" value=""True"" />
        <param name=""StaticLogFileName"" value=""True"" />
        <param name=""RollingStyle"" value=""Composite"" />
        <param name=""DatePattern"" value="".yyyy-MM-dd"" />
        <param name=""MaximumFileSize"" value=""1MB"" />
        <param name=""MaxSizeRollBackups"" value=""-1"" />
        <param name=""CountDirection"" value=""1"" />
        <layout type=""log4net.Layout.PatternLayout"">
            <conversionPattern value=""%date: %message%newline"" />
        </layout>
        <filter type=""log4net.Filter.LevelRangeFilter"">
            <levelMin value=""FATAL"" />
            <levelMax value=""FATAL"" />
        </filter>
    </appender>

    <root>
        <level value=""DEBUG"" />
        <appender-ref ref=""ApplicationAppender"" />
        <appender-ref ref=""DebugOnlyAppender"" />
        <appender-ref ref=""InfoOnlyAppender"" />
        <appender-ref ref=""WarnOnlyAppender"" />
        <appender-ref ref=""ErrorOnlyAppender"" />
        <appender-ref ref=""FatalOnlyAppender"" />
        <appender-ref ref=""ApplicationMessagesAppender"" />
        <appender-ref ref=""DebugOnlyMessagesAppender"" />
        <appender-ref ref=""InfoOnlyMessagesAppender"" />
        <appender-ref ref=""WarnOnlyMessagesAppender"" />
        <appender-ref ref=""ErrorOnlyMessagesAppender"" />
        <appender-ref ref=""FatalOnlyMessagesAppender"" />
    </root>
</log4net>";
        private static Stream GenerateStreamFromString(string s) {
            MemoryStream stream = new MemoryStream();
            StreamWriter writer = new StreamWriter(stream);
            writer.Write(s);
            writer.Flush();
            stream.Position = 0;
            return stream;
        }
    }
}
