﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace WebLogger {
    public class WebAppErrorLoggerHttpModule : IHttpModule {

        public void Init(HttpApplication webApp) {
            webApp.Error += WebApp_Error;
        }

        private void WebApp_Error(object sender, EventArgs e) {
            var context = sender as HttpApplication;
            var logger = LogService.GetLogger(sender.GetType());
            var excp = context.Server.GetLastError();
            // will logs all error, but if it's http error, only logs it if its status code is >= 500
            if ((!(excp is HttpException)) ||
                (excp is HttpException && ((HttpException)excp).GetHttpCode() >= 500)) {
                logger.Error(string.Format("Web app error ({0}): {1}", excp.GetType().FullName, excp.Message), excp);
            }
        }

        public void Dispose() {
            // none needed disposing
        }
    }
}
