﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

[assembly: PreApplicationStartMethod(typeof(WebLogger.HttpModuleRegistration), "RegisterModule")]

namespace WebLogger {
    /// <summary>
    /// This class is responsible to automatically register all HTTP modules required by this library
    /// </summary>
    public class HttpModuleRegistration {
        public static void RegisterModule() {
            HttpApplication.RegisterModule(typeof(WebAppErrorLoggerHttpModule));
        }
    }
}
