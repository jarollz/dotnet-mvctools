﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

[assembly: PreApplicationStartMethod(typeof(ObjectRenderer.Infrastructure.Http.HttpModuleRegistration), "RegisterModule")]

namespace ObjectRenderer.Infrastructure.Http {
    /// <summary>
    /// This class is responsible to automatically register all HTTP modules required by this library
    /// </summary>
    public class HttpModuleRegistration {
        public static void RegisterModule() {
            HttpApplication.RegisterModule(typeof(DefaultConfiguratorHttpModule));
            HttpApplication.RegisterModule(typeof(ErrorHandlerHttpModule));
        }
    }
}
