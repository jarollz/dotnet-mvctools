﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using ObjectTemplater.Models;
using ObjectTemplater.Services;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Hosting;

namespace ObjectRenderer.Infrastructure.Http {
    /// <summary>
    /// This HTTP module will do something when Application_Start event is fired to parse the config json
    /// in DEFAULT_CONFIGURATION_FILE_RELATIVE_PATH and provide default config for object renderer (and
    /// its underlying templater) that can then be used by HtmlHelper extensions method related to object
    /// rendering.
    /// Inspired by http://stackoverflow.com/a/3870590/426000 regarding handling Application_Start event in
    /// a http module.
    /// </summary>
    public class DefaultConfiguratorHttpModule : IHttpModule {

        const string DEFAULT_CONFIGURATION_FILE_RELATIVE_PATH = "~/templater.jsonconfig";

        private static bool _ApplicationStarted = false;

        private static object _ModuleStartLock = new object();

        /// <summary>
        /// Init this module
        /// </summary>
        /// <param name="context">HttpApplication object</param>
        public void Init(HttpApplication context) {
            lock (_ModuleStartLock) {
                if (!_ApplicationStarted) {
                    InitializeDefaultServices();
                    _ApplicationStarted = true;
                }
            }
        }

        private void InitializeDefaultServices() {
            var templaterConfig = ParseTemplaterConfigJson();
            ThemeService.DefaultTemplaterService = new TemplaterService(templaterConfig);
        }

        private TemplaterConfiguration ParseTemplaterConfigJson() {
            var cfgFile = HostingEnvironment.MapPath(DEFAULT_CONFIGURATION_FILE_RELATIVE_PATH);

            if (!File.Exists(cfgFile)) {
                throw new Exception(string.Format("Web app using ObjectRenderer must have a config file in {0}. The file {1} doesn't exists.", DEFAULT_CONFIGURATION_FILE_RELATIVE_PATH, cfgFile));
            }

            var cfgJson = File.ReadAllText(cfgFile);
            var cfgJs = JObject.Parse(cfgJson);

            string templateDirectory = cfgJs["TemplateDirectory"].ToString();
            if (templateDirectory.StartsWith("~")) {
                templateDirectory = HostingEnvironment.MapPath(templateDirectory);
            }

            var cfg = new TemplaterConfiguration(templateDirectory);

            if (cfgJs["Theme"] != null) {
                cfg.Theme = cfgJs["Theme"].ToString();
            }

            if (cfgJs["TemplateFileExtension"] != null) {
                cfg.TemplateFileExtension = cfgJs["TemplateFileExtension"].ToString();
            }

            if (cfgJs["TemplateFilePathsGenerators"] != null) {
                var pathGeneratorTypeNames = JsonConvert.DeserializeObject<List<string>>(cfgJs["TemplateFilePathsGenerators"].ToString());
                pathGeneratorTypeNames.Reverse();

                var allPathGenTypes = AppDomain.CurrentDomain.GetAssemblies()
                    .Select(asm => asm.GetTypes())
                    .Aggregate(new List<Type>(), (lst, typeArr) => { lst.AddRange(typeArr); return lst; })
                    .Where(type => type.GetInterfaces().Contains(typeof(ITemplateFilePathsGenerator)))
                    .ToArray();
                
                foreach (var typeName in pathGeneratorTypeNames) {
                    var matchingType = allPathGenTypes.Where(ty => ty.FullName == typeName).FirstOrDefault();
                    if (matchingType == null) {
                        throw new Exception(string.Format("Error in {0}: TemplateFilePathsGenerators must contain type full name that implements {1}. {2} is invalid.", cfgFile, typeof(ITemplateFilePathsGenerator).FullName, typeName));
                    }
                    
                    var pathGenInst = Activator.CreateInstance(matchingType) as ITemplateFilePathsGenerator;
                    cfg.AddTemplateFilePathsGenerator(pathGenInst, 0);
                }
            }

            return cfg;
        }

        /// <summary>
        /// Dispose used resources
        /// </summary>
        public void Dispose() {
            // none requires special action for disposing
        }
    }
}
