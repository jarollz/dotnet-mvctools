﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace ObjectRenderer.Infrastructure.Http {
    public class ErrorHandlerHttpModule : IHttpModule {

        public void Init(HttpApplication context) {
            if (Settings.EnableErrorHandling) {
                context.Error += RenderError;
            }
        }

        private void RenderError(object sender, EventArgs e) {
            var context = sender as HttpApplication;
            var excp = context.Server.GetLastError();
            var errTplAbsPath = ThemeService.DefaultTemplaterService.GetTemplateFilePathOrNullIfNotFound(excp, "view", ThemeService.Theme);

            if (errTplAbsPath != null) {
                var errTplPath = errTplAbsPath.ConvertAbsolutePathToRelativePath();
                
                context.Response.TrySkipIisCustomErrors = true;
                context.Response.ClearContent();

                context.Response.StatusCode = (excp is HttpException) ? (excp as HttpException).GetHttpCode() : 500;
                var html = DirectViewRenderer.RenderViewToString(errTplPath, excp, false);
                context.Response.Write(html);
                context.Response.End();
            }
        }

        public void Dispose() {
            // nothing needed disposing
        }
    }
}
