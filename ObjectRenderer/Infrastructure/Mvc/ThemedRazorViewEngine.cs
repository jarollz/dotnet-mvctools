﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace ObjectRenderer.Infrastructure.Mvc {
    /// <summary>
    /// Enhance razor view engine to use templater power.
    /// 
    /// <code>
    /// [templateRoot]
    ///   [themeName]
    ///     _mvc
    ///       [Controller namespace]
    ///         [Controller name]
    ///           [View name]
    /// </code>
    /// 
    /// Inspired by https://weblogs.asp.net/imranbaloch/view-engine-with-dynamic-view-location
    /// </summary>
    public class ThemedRazorViewEngine : RazorViewEngine {
        private ViewEngineLocationFormats defaultLocationFormats;
        /// <summary>
        /// Themed location formats for master view
        /// {0} is view name
        /// {1} is controller name
        /// </summary>
        public string[] ThemedMasterLocationFormats {
            get {
                return new string[] {
                    ":templateRoot:/:themeName:/_mvc/:controllerNamespace:/{1}/{0}.cshtml",
                    ":templateRoot:/:themeName:/_mvc/:controllerNamespace:/{1}/{0}.vbhtml",
                    ":templateRoot:/:themeName:/_mvc/_/{0}.cshtml",
                    ":templateRoot:/:themeName:/_mvc/_/{0}.vbhtml"
                };
            }
        }

        /// <summary>
        /// Themed location formats for area view
        /// {0} is view name
        /// {1} is controller name
        /// {2} is area name
        /// </summary>
        public string[] ThemedAreaLocationFormats {
            get {
                return new string[] {
                    ":templateRoot:/:themeName:/_mvc_{2}/:controllerNamespace:/{1}/{0}.cshtml",
                    ":templateRoot:/:themeName:/_mvc_{2}/:controllerNamespace:/{1}/{0}.vbhtml",
                    ":templateRoot:/:themeName:/_mvc_{2}/_/{0}.cshtml",
                    ":templateRoot:/:themeName:/_mvc_{2}/_/{0}.vbhtml"
                };
            }
        }

        public ThemedRazorViewEngine() {
            defaultLocationFormats = new ViewEngineLocationFormats(this);
        }

        protected override IView CreateView(ControllerContext controllerContext, string viewPath, string masterPath) {
            var ctrlNs = controllerContext.Controller.GetType().Namespace;
            var mp = masterPath.Replace(":controllerNamespace:", ctrlNs);
            return base.CreateView(controllerContext, viewPath, mp);
        }

        protected override IView CreatePartialView(ControllerContext controllerContext, string partialPath) {
            var ctrlNs = controllerContext.Controller.GetType().Namespace;
            var pp = partialPath.Replace(":controllerNamespace:", ctrlNs);
            return base.CreatePartialView(controllerContext, pp);
        }

        protected override bool FileExists(ControllerContext controllerContext, string virtualPath) {
            var ctrlNs = controllerContext.Controller.GetType().Namespace;
            var vp = virtualPath.Replace(":controllerNamespace:", ctrlNs);
            return base.FileExists(controllerContext, vp);
        }

        public override ViewEngineResult FindView(ControllerContext controllerContext, string viewName, string masterName, bool useCache) {
            ModifyLocationFormats(controllerContext);
            return base.FindView(controllerContext, viewName, masterName, useCache);
        }

        public override ViewEngineResult FindPartialView(ControllerContext controllerContext, string partialViewName, bool useCache) {
            ModifyLocationFormats(controllerContext);
            return base.FindPartialView(controllerContext, partialViewName, useCache);
        }

        private void ModifyLocationFormats(ControllerContext controllerContext) {
            if (IsThemeServiceAvailable()) {
                var tplRoot = GetTemplateRootAsRelativePath();
                var themeNames = GetThemeNames();
                var ctrlNs = controllerContext.Controller.GetType().Namespace;

                var amlf = new List<string>();
                foreach (var lf in ThemedAreaLocationFormats) {
                    foreach (var tn in themeNames) {
                        var nlf = lf.Replace(":templateRoot:", tplRoot)
                                    .Replace(":themeName:", tn)
                                    .Replace(":controllerNamespace:", ctrlNs);
                        amlf.Add(nlf);
                    }
                }

                var mlf = new List<string>();
                foreach (var lf in ThemedMasterLocationFormats) {
                    foreach (var tn in themeNames) {
                        var nlf = lf.Replace(":templateRoot:", tplRoot)
                                    .Replace(":themeName:", tn)
                                    .Replace(":controllerNamespace:", ctrlNs);
                        mlf.Add(nlf);
                    }
                }

                amlf.AddRange(defaultLocationFormats.AreaMasterLocationFormats);
                mlf.AddRange(defaultLocationFormats.MasterLocationFormats);

                AreaMasterLocationFormats = amlf.ToArray();
                AreaPartialViewLocationFormats = AreaMasterLocationFormats;
                AreaViewLocationFormats = AreaMasterLocationFormats;

                MasterLocationFormats = mlf.ToArray();
                PartialViewLocationFormats = MasterLocationFormats;
                ViewLocationFormats = MasterLocationFormats;
            } else {
                AreaMasterLocationFormats = defaultLocationFormats.AreaMasterLocationFormats;
                AreaPartialViewLocationFormats = defaultLocationFormats.AreaPartialViewLocationFormats;
                AreaViewLocationFormats = defaultLocationFormats.AreaViewLocationFormats;
                MasterLocationFormats = defaultLocationFormats.MasterLocationFormats;
                PartialViewLocationFormats = defaultLocationFormats.PartialViewLocationFormats;
                ViewLocationFormats = defaultLocationFormats.ViewLocationFormats;
            }
        }

        private string GetTemplateRootAsRelativePath() {
            return ThemeService.DefaultTemplaterService.Config.TemplateDirectory.ConvertAbsolutePathToRelativePath();
        }

        private IEnumerable<string> GetThemeNames() {
            return ThemeService.DefaultTemplaterService.GetValidThemeAlternatives(ThemeService.Theme);
        }

        private bool IsThemeServiceAvailable() {
            return ThemeService.DefaultTemplaterService != null;
        }
    }
}
