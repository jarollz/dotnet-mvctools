﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace ObjectRenderer.Infrastructure.Mvc {
    /// <summary>
    /// A generic controller to be used to render cshtml directly using its ControllerContext
    /// </summary>
    internal class GenericController : Controller {
    }
}
