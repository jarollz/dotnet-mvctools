﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace ObjectRenderer.Infrastructure.Mvc {
    internal class ViewEngineLocationFormats {
        public string[] AreaMasterLocationFormats { get; private set; }
        public string[] AreaPartialViewLocationFormats { get; private set; }
        public string[] AreaViewLocationFormats { get; private set; }
        public string[] MasterLocationFormats { get; private set; }
        public string[] PartialViewLocationFormats { get; private set; }
        public string[] ViewLocationFormats { get; private set; }

        public ViewEngineLocationFormats(VirtualPathProviderViewEngine viewEngine) {
            AreaMasterLocationFormats = viewEngine.AreaMasterLocationFormats;
            AreaPartialViewLocationFormats = viewEngine.AreaPartialViewLocationFormats;
            AreaViewLocationFormats = viewEngine.AreaViewLocationFormats;
            MasterLocationFormats = viewEngine.MasterLocationFormats;
            PartialViewLocationFormats = viewEngine.PartialViewLocationFormats;
            ViewLocationFormats = viewEngine.ViewLocationFormats;
        }
    }
}
