﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using System.Web.Mvc.Html;

namespace ObjectRenderer {
    /// <summary>
    /// Extension for HtmlHelper to provide PartialFromAbsolutePath and
    /// RenderPartialFromAbsolutePath methods.
    /// </summary>
    public static class HtmlHelperExt_PartialRender {
        /// <summary>
        /// Renders the specified partial view as an HTML-encoded string.
        /// </summary>
        /// <param name="htmlHelper">The HTML helper instance that this method extends.</param>
        /// <param name="partialViewName">The name of the partial view to render.</param>
        /// <returns>The partial view that is rendered as an HTML-encoded string.</returns>
        public static MvcHtmlString PartialFromAbsolutePath(this HtmlHelper htmlHelper, string partialViewName) {
            return PartialExtensions.Partial(htmlHelper, partialViewName.ConvertAbsolutePathToRelativePath());
        }
        
        /// <summary>
        /// Renders the specified partial view as an HTML-encoded string.
        /// </summary>
        /// <param name="htmlHelper">The HTML helper instance that this method extends.</param>
        /// <param name="partialViewName">The name of the partial view to render.</param>
        /// <param name="viewData">The view data dictionary for the partial view.</param>
        /// <returns>The partial view that is rendered as an HTML-encoded string.</returns>
        public static MvcHtmlString PartialFromAbsolutePath(this HtmlHelper htmlHelper, string partialViewName, ViewDataDictionary viewData) {
            return PartialExtensions.Partial(htmlHelper, partialViewName.ConvertAbsolutePathToRelativePath(), viewData);
        }

        /// <summary>
        /// Renders the specified partial view as an HTML-encoded string.
        /// </summary>
        /// <param name="htmlHelper">The HTML helper instance that this method extends.</param>
        /// <param name="partialViewName">The name of the partial view to render.</param>
        /// <param name="model">The model for the partial view.</param>
        /// <returns>The partial view that is rendered as an HTML-encoded string.</returns>
        public static MvcHtmlString PartialFromAbsolutePath(this HtmlHelper htmlHelper, string partialViewName, object model) {
            return PartialExtensions.Partial(htmlHelper, partialViewName.ConvertAbsolutePathToRelativePath(), model);
        }

        /// <summary>
        /// Renders the specified partial view as an HTML-encoded string.
        /// </summary>
        /// <param name="htmlHelper">The HTML helper instance that this method extends.</param>
        /// <param name="partialViewName">The name of the partial view.</param>
        /// <param name="model">The model for the partial view.</param>
        /// <param name="viewData">The view data dictionary for the partial view.</param>
        /// <returns>The partial view that is rendered as an HTML-encoded string.</returns>
        public static MvcHtmlString PartialFromAbsolutePath(this HtmlHelper htmlHelper, string partialViewName, object model, ViewDataDictionary viewData) {
            return PartialExtensions.Partial(htmlHelper, partialViewName.ConvertAbsolutePathToRelativePath(), model, viewData);
        }
        
        /// <summary>
        /// Renders the specified partial view by using the specified HTML helper.
        /// </summary>
        /// <param name="htmlHelper">The HTML helper.</param>
        /// <param name="partialViewName">The name of the partial view</param>
        public static void RenderPartialFromAbsolutePath(this HtmlHelper htmlHelper, string partialViewName) {
            RenderPartialExtensions.RenderPartial(htmlHelper, partialViewName.ConvertAbsolutePathToRelativePath());
        }

        /// <summary>
        /// Renders the specified partial view, replacing its ViewData property with the
        /// specified System.Web.Mvc.ViewDataDictionary object.
        /// </summary>
        /// <param name="htmlHelper">The HTML helper.</param>
        /// <param name="partialViewName">The name of the partial view.</param>
        /// <param name="viewData">The view data.</param>
        public static void RenderPartialFromAbsolutePath(this HtmlHelper htmlHelper, string partialViewName, ViewDataDictionary viewData) {
            RenderPartialExtensions.RenderPartial(htmlHelper, partialViewName.ConvertAbsolutePathToRelativePath(), viewData);
        }
        
        /// <summary>
        /// Renders the specified partial view, passing it a copy of the current System.Web.Mvc.ViewDataDictionary
        /// object, but with the Model property set to the specified model.
        /// </summary>
        /// <param name="htmlHelper">The HTML helper.</param>
        /// <param name="partialViewName">The name of the partial view.</param>
        /// <param name="model">The model.</param>
        public static void RenderPartialFromAbsolutePath(this HtmlHelper htmlHelper, string partialViewName, object model) {
            RenderPartialExtensions.RenderPartial(htmlHelper, partialViewName.ConvertAbsolutePathToRelativePath(), model);
        }
        
        /// <summary>
        /// Renders the specified partial view, replacing the partial view's ViewData property
        /// with the specified System.Web.Mvc.ViewDataDictionary object and setting the Model
        /// property of the view data to the specified model.
        /// </summary>
        /// <param name="htmlHelper">The HTML helper.</param>
        /// <param name="partialViewName">The name of the partial view.</param>
        /// <param name="model">The model for the partial view.</param>
        /// <param name="viewData">The view data for the partial view.</param>
        public static void RenderPartialFromAbsolutePath(this HtmlHelper htmlHelper, string partialViewName, object model, ViewDataDictionary viewData) {
            RenderPartialExtensions.RenderPartial(htmlHelper, partialViewName.ConvertAbsolutePathToRelativePath(), model, viewData);
        }
    }
}
