﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using System.Web.Mvc.Html;

namespace ObjectRenderer {
    /// <summary>
    /// Extension to HtmlHelper to allow rendering of partial views belonging to a layout of a theme
    /// </summary>
    public static class HtmlHelperExt_LayoutPartial {

        /// <summary>
        /// Renders the specified partial view as an HTML-encoded string.
        /// </summary>
        /// <param name="htmlHelper">The HTML helper instance that this method extends.</param>
        /// <param name="partialViewName">The name of the partial view to render.</param>
        /// <param name="themeName">The theme name, if null use ThemeService.Theme.</param>
        /// <returns>The partial view that is rendered as an HTML-encoded string.</returns>
        public static MvcHtmlString LayoutPartial(this HtmlHelper htmlHelper, string partialViewName, string themeName = null) {
            var tn = themeName == null ? ThemeService.Theme : themeName;
            return htmlHelper.Partial(ThemeService.GetFilePathInLayout(tn, partialViewName));
        }

        /// <summary>
        /// Renders the specified partial view as an HTML-encoded string.
        /// </summary>
        /// <param name="htmlHelper">The HTML helper instance that this method extends.</param>
        /// <param name="partialViewName">The name of the partial view to render.</param>
        /// <param name="viewData">The view data dictionary for the partial view.</param>
        /// <param name="themeName">The theme name, if null use ThemeService.Theme.</param>
        /// <returns>The partial view that is rendered as an HTML-encoded string.</returns>
        public static MvcHtmlString LayoutPartial(this HtmlHelper htmlHelper, string partialViewName, ViewDataDictionary viewData, string themeName = null) {
            var tn = themeName == null ? ThemeService.Theme : themeName;
            return htmlHelper.Partial(ThemeService.GetFilePathInLayout(tn, partialViewName), viewData);
        }

        /// <summary>
        /// Renders the specified partial view as an HTML-encoded string.
        /// </summary>
        /// <param name="htmlHelper">The HTML helper instance that this method extends.</param>
        /// <param name="partialViewName">The name of the partial view to render.</param>
        /// <param name="model">The model for the partial view.</param>
        /// <param name="themeName">The theme name, if null use ThemeService.Theme.</param>
        /// <returns>The partial view that is rendered as an HTML-encoded string.</returns>
        public static MvcHtmlString LayoutPartial(this HtmlHelper htmlHelper, string partialViewName, object model, string themeName = null) {
            var tn = themeName == null ? ThemeService.Theme : themeName;
            return htmlHelper.Partial(ThemeService.GetFilePathInLayout(tn, partialViewName), model);
        }

        /// <summary>
        /// Renders the specified partial view as an HTML-encoded string.
        /// </summary>
        /// <param name="htmlHelper">The HTML helper instance that this method extends.</param>
        /// <param name="partialViewName">The name of the partial view.</param>
        /// <param name="model">The model for the partial view.</param>
        /// <param name="viewData">The view data dictionary for the partial view.</param>
        /// <param name="themeName">The theme name, if null use ThemeService.Theme.</param>
        /// <returns>The partial view that is rendered as an HTML-encoded string.</returns>
        public static MvcHtmlString LayoutPartial(this HtmlHelper htmlHelper, string partialViewName, object model, ViewDataDictionary viewData, string themeName = null) {
            var tn = themeName == null ? ThemeService.Theme : themeName;
            return htmlHelper.Partial(ThemeService.GetFilePathInLayout(tn, partialViewName), model, viewData);
        }

        /// <summary>
        /// Renders the specified partial view by using the specified HTML helper.
        /// </summary>
        /// <param name="htmlHelper">The HTML helper.</param>
        /// <param name="partialViewName">The name of the partial view</param>
        /// <param name="themeName">The theme name, if null use ThemeService.Theme.</param>
        public static void RenderLayoutPartial(this HtmlHelper htmlHelper, string partialViewName, string themeName = null) {
            var tn = themeName == null ? ThemeService.Theme : themeName;
            htmlHelper.RenderPartial(ThemeService.GetFilePathInLayout(tn, partialViewName));
        }

        /// <summary>
        /// Renders the specified partial view, replacing its ViewData property with the
        /// specified System.Web.Mvc.ViewDataDictionary object.
        /// </summary>
        /// <param name="htmlHelper">The HTML helper.</param>
        /// <param name="partialViewName">The name of the partial view.</param>
        /// <param name="viewData">The view data.</param>
        /// <param name="themeName">The theme name, if null use ThemeService.Theme.</param>
        public static void RenderLayoutPartial(this HtmlHelper htmlHelper, string partialViewName, ViewDataDictionary viewData, string themeName = null) {
            var tn = themeName == null ? ThemeService.Theme : themeName;
            htmlHelper.RenderPartial(ThemeService.GetFilePathInLayout(tn, partialViewName), viewData);
        }

        /// <summary>
        /// Renders the specified partial view, passing it a copy of the current System.Web.Mvc.ViewDataDictionary
        /// object, but with the Model property set to the specified model.
        /// </summary>
        /// <param name="htmlHelper">The HTML helper.</param>
        /// <param name="partialViewName">The name of the partial view.</param>
        /// <param name="model">The model.</param>
        /// <param name="themeName">The theme name, if null use ThemeService.Theme.</param>
        public static void RenderLayoutPartial(this HtmlHelper htmlHelper, string partialViewName, object model, string themeName = null) {
            var tn = themeName == null ? ThemeService.Theme : themeName;
            htmlHelper.RenderPartial(ThemeService.GetFilePathInLayout(tn, partialViewName), model);
        }

        /// <summary>
        /// Renders the specified partial view, replacing the partial view's ViewData property
        /// with the specified System.Web.Mvc.ViewDataDictionary object and setting the Model
        /// property of the view data to the specified model.
        /// </summary>
        /// <param name="htmlHelper">The HTML helper.</param>
        /// <param name="partialViewName">The name of the partial view.</param>
        /// <param name="model">The model for the partial view.</param>
        /// <param name="viewData">The view data for the partial view.</param>
        /// <param name="themeName">The theme name, if null use ThemeService.Theme.</param>
        public static void RenderLayoutPartial(this HtmlHelper htmlHelper, string partialViewName, object model, ViewDataDictionary viewData, string themeName = null) {
            var tn = themeName == null ? ThemeService.Theme : themeName;
            htmlHelper.RenderPartial(ThemeService.GetFilePathInLayout(tn, partialViewName), model, viewData);
        }
    }
}
