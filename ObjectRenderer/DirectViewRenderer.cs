﻿using ObjectRenderer.Infrastructure.Mvc;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace ObjectRenderer {
    /// <summary>
    /// This class encapsulates the mechanism to render a view directly without undergoing all
    /// MVC lifecycle.
    /// </summary>
    public static class DirectViewRenderer {
        /// <summary>
        /// Render view to string
        /// </summary>
        /// <param name="context"></param>
        /// <param name="viewPath"></param>
        /// <param name="model"></param>
        /// <param name="partial"></param>
        /// <returns></returns>
        public static string RenderViewToString(this ControllerContext context,
                                    string viewPath,
                                    object model = null,
                                    bool partial = false) {
            // first find the ViewEngine for this view
            ViewEngineResult viewEngineResult = null;
            if (partial)
                viewEngineResult = ViewEngines.Engines.FindPartialView(context, viewPath);
            else
                viewEngineResult = ViewEngines.Engines.FindView(context, viewPath, null);

            if (viewEngineResult == null)
                throw new FileNotFoundException(string.Format("View {0} can't be found.", viewPath));

            // get the view and attach the model to view data
            var view = viewEngineResult.View;
            context.Controller.ViewData.Model = model;

            string result = null;

            using (var sw = new StringWriter()) {
                var ctx = new ViewContext(context, view,
                                            context.Controller.ViewData,
                                            context.Controller.TempData,
                                            sw);
                view.Render(ctx, sw);
                result = sw.ToString();
            }

            return result;
        }

        /// <summary>
        /// Render view to string
        /// </summary>
        /// <param name="controller"></param>
        /// <param name="viewPath"></param>
        /// <param name="model"></param>
        /// <param name="partial"></param>
        /// <returns></returns>
        public static string RenderViewToString(this Controller controller,
                                    string viewPath,
                                    object model = null,
                                    bool partial = false) {
            return RenderViewToString(controller.ControllerContext, viewPath, model, partial);
        }

        /// <summary>
        /// Render view to string using a throwaway controller
        /// </summary>
        /// <param name="viewPath"></param>
        /// <param name="model"></param>
        /// <param name="partial"></param>
        /// <returns></returns>
        public static string RenderViewToString(string viewPath,
                                    object model = null,
                                    bool partial = false) {
            var ctrl = CreateController<GenericController>();

            return RenderViewToString(ctrl, viewPath, model, partial);
        }

        /// <summary>
        /// Render view to string using a throwaway controller
        /// </summary>
        /// <param name="routeData"></param>
        /// <param name="viewPath"></param>
        /// <param name="model"></param>
        /// <param name="partial"></param>
        /// <returns></returns>
        public static string RenderViewToString(RouteData routeData, string viewPath,
                                    object model = null,
                                    bool partial = false) {
            var ctrl = CreateController<GenericController>();

            return RenderViewToString(ctrl, viewPath, model, partial);
        }

        /// <summary>
        /// Create an MVC controller directly
        /// </summary>
        /// <typeparam name="TController">The type of the controller</typeparam>
        /// <param name="routeData">route data</param>
        /// <returns>controller object</returns>
        public static TController CreateController<TController>(RouteData routeData = null)
            where TController : Controller, new() {
            // create a disconnected controller instance
            TController controller = new TController();

            // get context wrapper from HttpContext if available
            HttpContextBase wrapper;
            if (System.Web.HttpContext.Current != null)
                wrapper = new HttpContextWrapper(System.Web.HttpContext.Current);
            else
                throw new InvalidOperationException(
                    "Can't create Controller Context if no " +
                    "active HttpContext instance is available.");

            if (routeData == null)
                routeData = new RouteData();

            // add the controller routing if not existing
            if (!routeData.Values.ContainsKey("controller") &&
                !routeData.Values.ContainsKey("Controller"))
                routeData.Values.Add("controller",
                                     controller.GetType()
                                               .Name.ToLower().Replace("controller", ""));

            controller.ControllerContext = new ControllerContext(wrapper, routeData, controller);
            return controller;
        }
    }
}
