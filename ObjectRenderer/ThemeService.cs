﻿using ObjectTemplater.Services;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace ObjectRenderer {
    /// <summary>
    /// The ThemeService of ObjectRenderer. Used to set Theme and get the Layout to use when
    /// rendering controller action and objects for current request.
    /// </summary>
    public static class ThemeService {
        const string THEME_CONTEXT_KEY = "ObjectRenderer.ThemeService.Theme";
        const string LAYOUT_FOLDER = "_layouts";
        const string MAIN_LAYOUT_FILE_NAME = "_Layout.cshtml";

        /// <summary>
        /// The default templater service used when rendering object template. This prop is
        /// initialized on app start by a HTTP Module.
        /// </summary>
        public static ITemplaterService DefaultTemplaterService {
            get {
                return _DefaultTemplaterService;
            }
            set {
                if (_DefaultTemplaterService == null) {
                    _DefaultTemplaterService = value;
                } else {
                    throw new InvalidOperationException("ThemeService: DefaultTemplaterService can only be set once during app startup.");
                }
            }
        }
        private static ITemplaterService _DefaultTemplaterService = null;

        /// <summary>
        /// The theme name to use when finding layout and object template for current web request.
        /// </summary>
        public static string Theme {
            get {
                var theme = HttpContext.Current.Items[THEME_CONTEXT_KEY] as string;
                if (theme == null || theme.Trim().Length == 0) {
                    return DefaultTemplaterService.Config.Theme;
                }
                return theme;
            }
            set {
                HttpContext.Current.Items[THEME_CONTEXT_KEY] = value;
            }
        }

        /// <summary>
        /// The layout file to use for current web request. Depends on Theme property.
        /// </summary>
        public static string Layout {
            get {
                return GetMainLayoutFilePath();
            }
        }

        /// <summary>
        /// Get the main layout file path for the default theme
        /// </summary>
        /// <returns>The relative file path to the main layout file (_Layout.cshtml).</returns>
        public static string GetMainLayoutFilePath() {
            return GetFilePathInLayout(Theme, MAIN_LAYOUT_FILE_NAME);
        }

        /// <summary>
        /// Get the main layout file path for a given theme.
        /// </summary>
        /// <param name="themeName">The theme</param>
        /// <returns>The relative file path to the main layout file (_Layout.cshtml).</returns>
        public static string GetMainLayoutFilePath(string themeName) {
            return GetFilePathInLayout(themeName, MAIN_LAYOUT_FILE_NAME);
        }

        /// <summary>
        /// Get file path in the layout folder of a given theme.
        /// </summary>
        /// <param name="themeName">The theme</param>
        /// <param name="partialViewName">The file name</param>
        /// <returns>The relative file path to the file.</returns>
        public static string GetFilePathInLayout(string themeName, string partialViewName) {
            return Path.Combine(
                DefaultTemplaterService.Config.TemplateDirectory.ConvertAbsolutePathToRelativePath(),
                themeName,
                LAYOUT_FOLDER,
                partialViewName
            );
        }

        /// <summary>
        /// Use this on Application_Start in Global.asax.cs
        /// 
        /// This method will replace RazorViewEngine inside ViewEngines.Engines collection with
        /// ThemedRazorViewEngine of ObjectRenderer lib so that finding MVC views will also use
        /// templater power if needed.
        /// </summary>
        /// <param name="exclusive">If true, then use only ThemedRazorViewEngine and remove all
        /// other view engines.</param>
        public static void UseThemedRazorViewEngine(bool exclusive) {
            if (exclusive) {
                ViewEngines.Engines.Clear();
                ViewEngines.Engines.Add(new Infrastructure.Mvc.ThemedRazorViewEngine());
            } else {
                var hasRazorViewEngine = false;
                var replacedEngines = new List<IViewEngine>();
                foreach (var eng in ViewEngines.Engines) {
                    if (eng.GetType() == typeof(RazorViewEngine)) {
                        hasRazorViewEngine = true;
                        replacedEngines.Add(new Infrastructure.Mvc.ThemedRazorViewEngine());
                    } else {
                        replacedEngines.Add(eng);
                    }
                }
                if (!hasRazorViewEngine) {
                    replacedEngines.Add(new Infrastructure.Mvc.ThemedRazorViewEngine());
                }
                ViewEngines.Engines.Clear();
                foreach (var eng in replacedEngines) {
                    ViewEngines.Engines.Add(eng);
                }
            }
        }
    }
}
