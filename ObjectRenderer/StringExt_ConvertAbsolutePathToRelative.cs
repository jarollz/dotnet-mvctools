﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Hosting;

namespace ObjectRenderer {
    /// <summary>
    /// Extension to string to convert absolute path to web relative path
    /// </summary>
    public static class StringExt_ConvertAbsolutePathToRelative {
        /// <summary>
        /// Convert absolute path to ~ relative path. If it can't then, just return the original
        /// input.
        /// </summary>
        /// <param name="absolutePath"></param>
        /// <returns>relative path</returns>
        public static string ConvertAbsolutePathToRelativePath(this string absolutePath) {
            var basePath = HostingEnvironment.MapPath("~");
            var relPath = absolutePath.Replace(basePath, "");
            if (relPath.Length < absolutePath.Length) {
                var virPath = Path.Combine("~", relPath);
                virPath = virPath.Replace("\\", "/"); // make the it like the web
                return virPath;
            }
            return absolutePath;
        }
    }
}
