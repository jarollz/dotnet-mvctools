﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Configuration;

namespace ObjectRenderer {
    public static class Settings {
        public const string HANDLE_ERROR_SETTING = "ObjectRenderer:EnableErrorHandling";

        public static bool EnableErrorHandling {
            get {
                var appStg = WebConfigurationManager.AppSettings[HANDLE_ERROR_SETTING];
                var handleError = true;
                if (appStg != null) {
                    bool.TryParse(appStg, out handleError);
                }
                return handleError;
            }
        }
    }
}
