﻿using ObjectTemplater.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace ObjectRenderer.Models {
    /// <summary>
    /// Template file paths generator for error. Will use 4xx or 5xx cshtml.
    /// </summary>
    public class ErrorTemplateFilePathsGenerator : ITemplateFilePathsGenerator {
        public const string ERROR_TEMPLATE_FOLDER = "_errors";

        public IEnumerable<string> GenerateTemplateFilePaths(Type type, string renderMode, IEnumerable<string> themeNames, string rootTemplateDirectory, string templateFileExtension) {
            return new string[0];
        }

        public IEnumerable<string> GenerateTemplateFilePaths(object obj, string renderMode, IEnumerable<string> themeNames, string rootTemplateDirectory, string templateFileExtension) {
            if (obj is Exception) {
                foreach (var possibleTheme in themeNames) {
                    foreach (var rm in (new string[] { renderMode, "" })) {
                        foreach (var errFolder in GetAllPossibleErrorFolders(obj)) {
                            var templateFilePath = Path.Combine(
                                    rootTemplateDirectory,
                                    possibleTheme,
                                    ERROR_TEMPLATE_FOLDER,
                                    errFolder,
                                    "_" + rm + "." + templateFileExtension
                                );
                            yield return templateFilePath;
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Convert status code to a list of render names. It converts status code e.g. 500
        /// to [500, 50x, 5xx, xxx]
        /// </summary>
        /// <param name="obj">The error object</param>
        /// <returns>possible render names</returns>
        private IEnumerable<string> GetAllPossibleErrorFolders(object obj) {
            int statusCode = 500;
            if (obj is HttpException) {
                statusCode = (obj as HttpException).GetHttpCode();
            }

            var statusCodeCharArray = statusCode.ToString().ToCharArray();

            for (int i = 0; i < statusCodeCharArray.Length; i++) {
                var scs = new char[statusCodeCharArray.Length];
                Array.Copy(statusCodeCharArray, scs, statusCodeCharArray.Length);
                var countToReplace = i;
                while (countToReplace > 0) {
                    for (var j = statusCodeCharArray.Length - 1; j > statusCodeCharArray.Length - 1 - countToReplace; j--) {
                        scs[j] = 'x';
                    }
                    countToReplace--;
                }
                yield return new string(scs);
            }
            yield return "xxx";
        }
    }
}
