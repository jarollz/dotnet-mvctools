﻿using ObjectTemplater.Services;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace ObjectRenderer {
    /// <summary>
    /// Extensions to HtmlHelper to provide ObjectRenderingFor method.
    /// </summary>
    public static class HtmlHelperExt_ObjectRenderer {

        /// <summary>
        /// Render an object using ObjectRenderer + ObjectTemplater power
        /// </summary>
        /// <typeparam name="TModel">The model type of the template</typeparam>
        /// <typeparam name="TProperty">The property type of a property in the model</typeparam>
        /// <param name="htmlHelper">The html helper associated with the template</param>
        /// <param name="modelPropertyExpression">The expression that can navigate to the appropriate property inside the model</param>
        /// <param name="chainModelProperty">If true, then propagate all model related data to the resulting rendered template. Useful for rendering form fields as it can help this method behave like EditorFor method of ASP MVC. Defaults to false, meaning every rendered template will have it's own model state unrelated to the parent/caller template model state.</param>
        /// <param name="renderMode">The render mode, if null then 'view' is assumed</param>
        /// <param name="customPropagatedHtmlFieldPrefix">Custom html field prefix to use. If null, then use old prefix + generated prefix, if not null then use old prefix + custom prefix</param>
        /// <param name="additionalViewData">Additional view data to be put inside the rendered template view data if any</param>
        /// <param name="templaterService">The templater service to use that can find a suitable template for the object. If null, then ObjectRenderer.Models.DefaultServices.DefaultTemplaterService will be used (this static object is initialized according to config at app startup).</param>
        /// <returns>Object rendered using proper template</returns>
        public static MvcHtmlString ObjectRenderingFor<TModel, TProperty>(this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, TProperty>> modelPropertyExpression, bool chainModelProperty = false, string renderMode = null, string customPropagatedHtmlFieldPrefix = null, IDictionary<string, object> additionalViewData = null, ITemplaterService templaterService = null) {
            var ts = GetProperTemplaterServiceIfNeeded(templaterService);
            var propertyData = RetrieveProperty(modelPropertyExpression, htmlHelper.ViewData.Model);
            var tpl = propertyData.Item1 != null ? ts.GetTemplateFilePath(propertyData.Item1, renderMode, ThemeService.Theme)
                                                 : ts.GetTemplateFilePath(propertyData.Item3, renderMode, ThemeService.Theme);
            var vd = new ViewDataDictionary<TProperty>();
            foreach (var hvdkvp in htmlHelper.ViewData) {
                vd.Add(hvdkvp);
            }
            // chain model metadata
            vd.ModelMetadata = ModelMetadata.FromLambdaExpression(modelPropertyExpression, htmlHelper.ViewData);
            vd.Model = propertyData.Item1;

            if (chainModelProperty) {
                // chain html field prefix
                var htmlFieldPrefix = string.Join(".", propertyData.Item2);

                if (customPropagatedHtmlFieldPrefix != null && customPropagatedHtmlFieldPrefix.Trim().Length > 0) {
                    htmlFieldPrefix = customPropagatedHtmlFieldPrefix;
                }
                vd.TemplateInfo = new TemplateInfo() {
                    HtmlFieldPrefix = htmlFieldPrefix
                };
                if (htmlHelper.ViewData.TemplateInfo != null) {
                    var oldPrefix = htmlHelper.ViewData.TemplateInfo?.HtmlFieldPrefix?.TrimEnd('.');
                    if (oldPrefix != null && oldPrefix.Length > 0) {
                        if (vd.TemplateInfo.HtmlFieldPrefix.StartsWith("[")) {
                            vd.TemplateInfo.HtmlFieldPrefix = oldPrefix + vd.TemplateInfo.HtmlFieldPrefix;
                        } else {
                            vd.TemplateInfo.HtmlFieldPrefix = oldPrefix + "." + vd.TemplateInfo.HtmlFieldPrefix;
                        }
                    }
                }

                // chain model state
                foreach (var mskvp in htmlHelper.ViewData.ModelState) {
                    vd.ModelState.Add(mskvp);
                }
            }
            if (additionalViewData != null) {
                foreach (var avdkvp in additionalViewData) {
                    vd.Add(avdkvp);
                }
            }
            return htmlHelper.PartialFromAbsolutePath(tpl, propertyData.Item1, vd);
        }

        /// <summary>
        /// Render an object using ObjectRenderer + ObjectTemplater power
        /// </summary>
        /// <typeparam name="TModel">The model type of the template</typeparam>
        /// <typeparam name="TProperty">The property type of a property in the model</typeparam>
        /// <param name="htmlHelper">The html helper associated with the template</param>
        /// <param name="model">The model object that will be consulted to get its property for rendering</param>
        /// <param name="modelPropertyExpression">The expression that can navigate to the appropriate property inside the model</param>
        /// <param name="chainModelProperty">If true, then propagate all model related data to the resulting rendered template. Useful for rendering form fields as it can help this method behave like EditorFor method of ASP MVC. Defaults to false, meaning every rendered template will have it's own model state unrelated to the parent/caller template model state.</param>
        /// <param name="renderMode">The render mode, if null then 'view' is assumed</param>
        /// <param name="customPropagatedHtmlFieldPrefix">Custom html field prefix to use. If null, then use old prefix + generated prefix, if not null then use old prefix + custom prefix</param>
        /// <param name="additionalViewData">Additional view data to be put inside the rendered template view data if any</param>
        /// <param name="templaterService">The templater service to use that can find a suitable template for the object. If null, then ObjectRenderer.Models.DefaultServices.DefaultTemplaterService will be used (this static object is initialized according to config at app startup).</param>
        /// <returns>Object rendered using proper template</returns>
        public static MvcHtmlString ObjectRenderingFor<TModel, TProperty>(this HtmlHelper htmlHelper, TModel model, Expression<Func<TModel, TProperty>> modelPropertyExpression, bool chainModelProperty = false, string renderMode = null, string customPropagatedHtmlFieldPrefix = null, IDictionary<string, object> additionalViewData = null, ITemplaterService templaterService = null) {
            var ts = GetProperTemplaterServiceIfNeeded(templaterService);
            var propertyData = RetrieveProperty(modelPropertyExpression, model);
            var tpl = propertyData.Item1 != null ? ts.GetTemplateFilePath(propertyData.Item1, renderMode, ThemeService.Theme)
                                                 : ts.GetTemplateFilePath(propertyData.Item3, renderMode, ThemeService.Theme);
            var vd = new ViewDataDictionary<TProperty>();
            foreach (var hvdkvp in htmlHelper.ViewData) {
                vd.Add(hvdkvp);
            }
            // chain model metadata
            vd.ModelMetadata = ModelMetadataProviders.Current.GetMetadataForType(() => { return propertyData.Item1; }, propertyData.Item3);
            vd.Model = propertyData.Item1;

            if (chainModelProperty) {
                // chain html field prefix
                var htmlFieldPrefix = string.Join(".", propertyData.Item2);

                if (customPropagatedHtmlFieldPrefix != null && customPropagatedHtmlFieldPrefix.Trim().Length > 0) {
                    htmlFieldPrefix = customPropagatedHtmlFieldPrefix;
                }
                vd.TemplateInfo = new TemplateInfo() {
                    HtmlFieldPrefix = htmlFieldPrefix
                };
                if (htmlHelper.ViewData.TemplateInfo != null) {
                    var oldPrefix = htmlHelper.ViewData.TemplateInfo?.HtmlFieldPrefix?.TrimEnd('.');
                    if (oldPrefix != null && oldPrefix.Length > 0) {
                        if (vd.TemplateInfo.HtmlFieldPrefix.StartsWith("[")) {
                            vd.TemplateInfo.HtmlFieldPrefix = oldPrefix + vd.TemplateInfo.HtmlFieldPrefix;
                        } else {
                            vd.TemplateInfo.HtmlFieldPrefix = oldPrefix + "." + vd.TemplateInfo.HtmlFieldPrefix;
                        }
                    }
                }

                // chain model state
                foreach (var mskvp in htmlHelper.ViewData.ModelState) {
                    vd.ModelState.Add(mskvp);
                }
            }
            if (additionalViewData != null) {
                foreach (var avdkvp in additionalViewData) {
                    vd.Add(avdkvp);
                }
            }
            return htmlHelper.PartialFromAbsolutePath(tpl, propertyData.Item1, vd);
        }

        private static ITemplaterService GetProperTemplaterServiceIfNeeded(ITemplaterService templaterService) {
            if (templaterService != null) {
                return templaterService;
            }
            return ThemeService.DefaultTemplaterService;
        }

        /// <summary>
        /// Get object property according to expression and also it's property name chain
        /// </summary>
        /// <typeparam name="T">The object type</typeparam>
        /// <typeparam name="P">The property type</typeparam>
        /// <param name="expr">The expression to retrieve the property from the object</param>
        /// <param name="model">The model whose property is being retrieved</param>
        /// <returns>A tuple, Item1 is the property object, Item2 is the string that contains the chain of property names from object to the retrieved property, Item3 is the Type of the property</returns>
        private static Tuple<P, IEnumerable<string>, Type> RetrieveProperty<T, P>(Expression<Func<T, P>> expr, T model) {
            MemberExpression me;
            switch (expr.Body.NodeType) {
                case ExpressionType.Convert:
                case ExpressionType.ConvertChecked:
                    var ue = expr.Body as UnaryExpression;
                    me = ((ue != null) ? ue.Operand : null) as MemberExpression;
                    break;
                default:
                    me = expr.Body as MemberExpression;
                    break;
            }

            var nameChain = new List<string>();
            P currentPropertyObject = default(P);
            if (model != null) currentPropertyObject = expr.Compile()(model);
            while (me != null) {
                string propertyName = me.Member.Name;
                Type propertyType = me.Type;

                //Debug.WriteLine(propertyName + ": " + propertyType);
                nameChain.Add(propertyName);
                me = me.Expression as MemberExpression;
            }
            return Tuple.Create(currentPropertyObject, nameChain.AsEnumerable(), typeof(P));
        }
    }
}
