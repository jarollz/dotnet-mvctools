﻿# MVC Tools #

## Overview ##

Libraries to be used in MVC web app to ease development.

## Libraries ##

1.  ObjectTemplater

    Find suitable template given an object or a type, while considering its inheritance hierarchy
    and some common interfaces (IDictionary, ICollection, IEnumrable) that supports all kinds of
    types even generic types or even generic types of generic types.
    This lib doesn't normally need to be used directly by ASP MVC app.

2.  ObjectRenderer
    
    Render the template of an object found by ObjectTemplater in ASP MVC web app environment, while
    giving support for model state and metadata propagation in the case of nested template (e.g.
    rendering a form, and then render its fields separately - thus nested).
    ASP MVC app will need to provide/use:
    - Add @using ObjectRenderer in all cshtml that needs to use the renderer power.
    - Create ~/templater.jsonconfig for configuring the renderer.
    - Create ~/Views/default theme folder with a certain unique structure. When the renderer is
      being used, if it can't find appropriate template for an object or a type, it'll show a list
      of searched template file names that can then be used by the dev to create an appropriate
      template.

3.  TemplateSectionRenderer
    
    Razor partial view can't use section like @section scripts { ... }. This statement can only be
    used by the controller view. This lib will provide a similar power for all cshtml be it a view
    or a partial.
    ASP MVC app will use this lib by doing:
    - In all cshtml that want to use this lib power, add @using TemplateSectionRenderer
    - In ~/Views/Shared/_Layout.cshtml, call @Html.RenderSection("someSectionName") to be a
      placeholder for someSectionName.
    - All other cshtml can add to someSectionName by making a statement like
      @using (Html.Section("someSectionName") { ... }. The '...' can then be filled with
      HTML/Razor C# codes.

4.  WebLogger
    
    Provides LogService using Log4Net. Will automatically get configuration from ~/log4net.config.
    If it doesn't exits, the system will attempt to create it. If it can't, then use default
    log4net config that is very powerful and informative. Inside this lib, there's also a http
    module that will automatically register itself into the webapp and listen to Application_Error
    event, and logs any kinds of Exception and HttpException with status code >= 500. Will store
    all logs in ~/App_Data/Logs/*.log

5.  FileUploadHelpers
    
    Provides easy helper methods to convert relative paths (e.g.
    `~/some-folder/another-folder/a-file.cshtml`) to absolute path, also for checking file
    existence, deletion, etc using `FileHelper` class. Also provides file upload handling that can
    save posted file from HTML to backend with automatic renaming to prevent filename collision
    by `using FileUploadHelpers`. When that using statement is put at the top, the extension to
    `HttpPostedFileBase` providing powerful `SavePostedFile` method will be available immediately.

6.  FlashMessageHelpers
    
    Provide extensions to MVC Controller and Razor WebViewPage to add or show flash message, i.e.
    a message that should survive multiple page request but after being shown then remove it. Good
    for showing a kind of notification to the user from any web page. To use, add
    `using FlashMessageHelpers` above Controller class to have the extension to add/clear flash
    messages, and add `@using FlashMessageHelpers` to layout cshtml file to get and show the flash
    messages.

7.  MvcActionIpFilter
    
    Filter controller action access according to visitor IP address defined in Web.config. To use, 
    first define the ip filter config by adding and modifying sample config in
    `Sample-AddToWebConfig.txt`. And then attach `[ActionIpFilter("RuleName")] to the action that
    you want to filter. Change `RuleName` in the attribute parameter to the rule that you want to
    use according to your defined web.config.

## Dev Notes ##

-   If want to use Ninject for web app with MVC and WebAPI, refer to this
    http://stackoverflow.com/a/22611146/426000

    In the case you start an IIS MVC5 web app from scratch and want to use Ninject install the
    following packages:

    * Ninject - Ninject core dll
    * Ninject.Web.Common - Common Web functionnality for Ninject eg. InRequestScope()
    * Ninject.MVC5 - MVC dependency injectors eg. to provide Controllers for MVC
    * Ninject.Web.Common.WebHost - Registers the dependency injectors from Ninject.MVC5 when IIS
    starts the web app. If you are not using IIS you will need a different package, check above
    * Ninject.Web.WebApi WebApi dependency injectors eg. to provide Controllers for WebApi, the
    version must be >= 3.2.4.0 to avoid double binding on HttpConfiguration
    * Ninject.web.WebApi.WebHost - Registers the dependency injectors from Ninject.Web.WebApi when
    IIS starts the web app.

## Developer ##

Roland L. Bu'ulölö