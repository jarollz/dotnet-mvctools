﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace FlashMessageHelpers {
    public class FlashMessages {

        public void AddFlashMessage(string message) {
            var fms = GetFlashMessages();
            fms.Add(message);
        }
        public bool HasFlashMessage() {
            return GetFlashMessages().Count > 0;
        }

        public ICollection<string> GetAndClearFlashMessages() {
            var fms = GetFlashMessages().ToList();
            ClearFlashMessages();
            return fms;
        }

        public ICollection<string> GetFlashMessages() {
            if (Session != null) {
                var fms = Session[tempDataKey] as ICollection<string>;
                if (fms == null) {
                    fms = new List<string>();
                    Session[tempDataKey] = fms;
                }
                return fms;
            } else {
                return new List<string>();
            }
        }

        
        public void ClearFlashMessages() {
            if (Session != null)
                Session.Remove(tempDataKey);
        }

        public static FlashMessages Get() {
            return new FlashMessages(new HttpContextWrapper(HttpContext.Current));
        }

        private FlashMessages(HttpContextBase httpContext) {
            this.httpContext = httpContext;
            if (Session == null) {
                tempDataKey = "FlashMessageHelpers:TempDataKey";
            } else {
                tempDataKey = string.Format("FlashMessageHelpers:TempDataKey_{0}", Session.SessionID);
            }
        }

        private HttpSessionStateBase Session {
            get {
                return httpContext.Session;
            }
        }
        private HttpContextBase httpContext;
        private string tempDataKey;
    }
}
