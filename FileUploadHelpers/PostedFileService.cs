﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Hosting;

namespace FileUploadHelpers {
    public static class PostedFileService {
        public static PostedFileInfo SavePostedFile(HttpPostedFileBase postedFile,
            string saveLocationRelativeDirectory,
            string specificSaveFileName = null) {

            if (!IsExistsAndNotEmpty(postedFile))
                return new PostedFileInfo(null, null);

            var relativeUploadDir = MakeDirectoryRelativeToServer(saveLocationRelativeDirectory);
            var fullUploadDir = GetAbsoluteDirectoryAndCreateIfNotExists(relativeUploadDir);
            var saveFileName = specificSaveFileName == null ? GenerateRandomFileName() : specificSaveFileName;
            var cleanSaveFileName = GetFileNameWithCorrectExtension(saveFileName, postedFile);
            var relativeFilePath = Path.Combine(relativeUploadDir, cleanSaveFileName)
                                       .Replace("\\", "/")
                                       .TrimStart('~');
            var fullFilePath = Path.Combine(fullUploadDir, cleanSaveFileName).Replace("/", "\\");
            postedFile.SaveAs(fullFilePath);

            return new PostedFileInfo(fullFilePath, relativeFilePath);
        }

        public static bool IsExistsAndNotEmpty(HttpPostedFileBase postedFile) {
            return postedFile != null && postedFile.ContentLength > 0;
        }

        private static string MakeDirectoryRelativeToServer(string directory) {
            var uploadDir = directory.Replace("\\", "/")
                                     .TrimStart('~')
                                     .TrimStart('/')
                                     .TrimStart('\\');
            return string.Format("~/{0}", uploadDir);
        }

        public static string GetAbsoluteDirectoryAndCreateIfNotExists(string directoryRelativeToServer) {
            var uploadDir = HostingEnvironment.MapPath(directoryRelativeToServer);
            if (!Directory.Exists(uploadDir)) {
                Directory.CreateDirectory(uploadDir);
            }
            return uploadDir;
        }

        public static string GenerateRandomFileName() {
            return Settings.GenerateRandomFileNameMethod.Invoke();
        }

        private static string GetFileNameWithCorrectExtension(string fileName, HttpPostedFileBase postedFile) {
            FileInfo finfo = new FileInfo(postedFile.FileName);
            string correctFileExtension = finfo.Extension.ToLower();

            if (!fileName.EndsWith(correctFileExtension)) {
                return fileName + correctFileExtension;
            }
            return fileName;
        }
    }

    public class PostedFileInfo {
        public string FullFilePath { get; protected set; }
        public string RelativeFilePath { get; protected set; }

        public PostedFileInfo(string fullFilePath, string relativeFilePath) {
            FullFilePath = fullFilePath;
            RelativeFilePath = relativeFilePath;
        }
    }
}