﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace FileUploadHelpers {
    [AttributeUsage(AttributeTargets.Property | AttributeTargets.Field, AllowMultiple = false, Inherited = true)]
    public class ValidatePostedFileAttribute : ValidationAttribute {
        /// <summary>
        /// Minimum file size in bytes
        /// </summary>
        public int MinFileSizeBytes { get; set; }
        /// <summary>
        /// Maximum file size in bytes
        /// </summary>
        public int MaxFileSizeBytes { get; set; }
        /// <summary>
        /// An array of mime type strings to validate against. If null or empty, then accept all
        /// mime types.
        /// </summary>
        public string[] MimeTypes {
            get {
                if (_MimeTypes == null) {
                    _MimeTypes = new string[0];
                }
                return _MimeTypes;
            }
            set {
                _MimeTypes = value;
            }
        }
        private string[] _MimeTypes;

        protected ValidatePostedFileAttribute(Func<string> errorMessageAccessor) : base(errorMessageAccessor) {
        }

        protected ValidatePostedFileAttribute(string errorMessage) : base(errorMessage) {
        }

        protected override ValidationResult IsValid(object value, ValidationContext validationContext) {
            if (value == null) return ValidationResult.Success;
            if (!typeof(HttpPostedFileBase).IsAssignableFrom(value.GetType())) {
                throw new InvalidOperationException($"ValidatePostedFileAttribute: Can't validate field or property whose base/class is not {typeof(HttpPostedFileBase).FullName}.");
            }

            var val = value as HttpPostedFileBase;
            var memberNames = new string[] { validationContext.MemberName };
            if (val.ContentLength < MinFileSizeBytes) {
                SetErrorMessageIfNull($"File size in {{0}} field must be greater than or equal to {MinFileSizeBytes} bytes.");
                return new ValidationResult(FormatErrorMessage(validationContext.DisplayName), memberNames);
            }
            if (val.ContentLength > MaxFileSizeBytes) {
                SetErrorMessageIfNull($"File size in {{0}} field must be less than or equal to {MaxFileSizeBytes} bytes.");
                return new ValidationResult(FormatErrorMessage(validationContext.DisplayName), memberNames);
            }
            if (MimeTypes.Length > 0) {
                if (!MimeTypes.Contains(val.ContentType)) {
                    SetErrorMessageIfNull($"File mime type in {{0}} field must be one of {string.Join(", or ", MimeTypes)}.");
                    return new ValidationResult(FormatErrorMessage(validationContext.DisplayName), memberNames);
                }
            }
            return ValidationResult.Success;
        }

        public ValidatePostedFileAttribute() {
            MinFileSizeBytes = 0;
            MaxFileSizeBytes = int.MaxValue;
            MimeTypes = new string[0];
            ErrorMessage = null;
        }

        private void SetErrorMessageIfNull(string errorMessage) {
            if (ErrorMessage == null || ErrorMessage.Trim().Length == 0) {
                ErrorMessage = errorMessage;
            }
        }
    }
}
