﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Hosting;

namespace FileUploadHelpers {
    public static class FileHelper {
        public static string GetAbsoluteFilePath(string serverRelativeFilePath) {
            if (!string.IsNullOrWhiteSpace(serverRelativeFilePath)) {
                var srfp = serverRelativeFilePath;
                if (!srfp.StartsWith("~/")) {
                    srfp = Path.Combine("~", srfp);
                }
                return HostingEnvironment.MapPath(srfp);
            }
            return null;
        }

        public static bool IsExists(string serverRelativeFilePath) {
            if (!string.IsNullOrWhiteSpace(serverRelativeFilePath)) {
                var truePath = GetAbsoluteFilePath(serverRelativeFilePath);
                if (File.Exists(truePath)) {
                    return true;
                }
            }
            return false;
        }

        public static bool Delete(string serverRelativeFilePath) {
            try {
                if (IsExists(serverRelativeFilePath)) {
                    File.Delete(GetAbsoluteFilePath(serverRelativeFilePath));
                    return true;
                }
            } catch (IOException) {
                return false;
            }
            return false;
        }

        public static string GetFileName(string serverRelativeFilePath) {
            var truePath = GetAbsoluteFilePath(serverRelativeFilePath);
            if (!string.IsNullOrWhiteSpace(truePath)) {
                return Path.GetFileName(truePath);
            }
            return null;
        }

        public static string GetFileNameWithoutExtension(string serverRelativeFilePath) {
            var truePath = GetAbsoluteFilePath(serverRelativeFilePath);
            if (!string.IsNullOrWhiteSpace(truePath)) {
                return Path.GetFileNameWithoutExtension(truePath);
            }
            return null;
        }
    }
}