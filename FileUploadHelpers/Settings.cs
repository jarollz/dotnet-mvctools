﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FileUploadHelpers {
    public static class Settings {
        public static Func<string> GenerateRandomFileNameMethod { get; set; }

        public static string DefaultGenerateRandomFileNameMethod() {
            return string.Format("File-{0}-{1}", DateTime.Now.ToString("yyyyMMddHHmmss"), Guid.NewGuid().ToString());
        }

        static Settings() {
            GenerateRandomFileNameMethod = DefaultGenerateRandomFileNameMethod;
        }
    }
}
